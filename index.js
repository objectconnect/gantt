// Load polyfills ( cssvars for themeing )
require('./src/polyfills/polyfill.js');
require('./src/polyfills/cssvars.js');

// Load control entry point
require('./src/timeline.js');

// Load helpers, DOM elements temeplates and control confiugration
require('./src/helpers/helpers.js');
require('./src/helpers/movement.js');
require('./src/templates/templates.js');
require('./src/configuration/config.js');

// Load control components
require('./src/components/header.js');
require('./src/components/listheader.js');
require('./src/components/columns.js');
require('./src/components/connection.js');
require('./src/components/connections.js');
require('./src/components/group.js');
require('./src/components/spacer.js');
require('./src/components/item.js');
require('./src/components/items.js');
require('./src/components/viewport.js');

// Load default theme
require('./src/theme/default.css');

// Load timeline styles
require('./src/styles/timeline.css');

module.exports = globalThis.Timeline;