if (!Array.prototype.move) {
    Array.prototype.move = function(from,to){
        this.splice(to,0,this.splice(from,1)[0]);
        return this;
    };
}

export function setSizeInPX(value) { return value + "px"; }

export function getMonthDiff (d1, d2) {
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

export function getYearDiff(d1, d2) 
{
    let years = (d2.getTime() - d1.getTime()) / 1000;
    years /= (60 * 60 * 24);
    return Math.abs(Math.round(years/365.25));
}

if (!Date.prototype.getQuarter) {
    Date.prototype.getQuarter = function getQuarter() {
        //this = this || new Date(); // If no date supplied, use today
        var q = [0,1,2,3];
        return q[Math.floor(this.getMonth() / 3)];
    }
}

if (!Date.prototype.getHalfYear) {
    Date.prototype.getHalfYear = function getHalfYear() {
        //this = this || new Date(); // If no date supplied, use today
        var q = [0,1];
        return q[Math.floor(this.getMonth() / 6)];
    }
}

if (!Date.prototype.getWeekNumber) {
    Date.prototype.getWeekNumber = function(){
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((new Date(this.getFullYear(), this.getMonth(), this.getDate()) - onejan) / 86400000) + onejan.getDay() + 1) / 7);
        
    };
}

export function dynamicallyLoadScript(url) {
    var script = document.createElement("script");  // create a script DOM node
    script.src = url;  // set its src to the provided URL

    document.head.appendChild(script);  // add it to the end of the head section of the page (could change 'head' to 'body' to add it to the end of the body section instead)
}

export function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}

export function getDateOfWeek(w, y) {
    var date = new Date(y, 0, (1 + (w - 1) * 7));
    date.setDate(date.getDate() + (1 - date.getDay())); // 0 - Sunday, 1 - Monday etc
    return date;
}

export const weeksCount = function(year, month_number) {
    var firstOfMonth = new Date(year, month_number - 1, 1);
    var day = firstOfMonth.getDay() || 6;
    day = day === 1 ? 0 : day;
    if (day) { day-- }
    var diff = 7 - day;
    var lastOfMonth = new Date(year, month_number, 0);
    var lastDate = lastOfMonth.getDate();
    if (lastOfMonth.getDay() === 1) {
        diff--;
    }
    var result = Math.ceil((lastDate - diff) / 7);
    return result + 1;
};

export function describeArc(x, y, radius, startAngle, endAngle) {
    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);

    var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

    var d = [
        "M", start.x, start.y, 
        "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
    ].join(" ");

    return d; 
}

export function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    }
}