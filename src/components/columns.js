import {
    getDateFromId,
    calculateNewDate,
    generateFromTemplate,
    getColumnContent,
    getIdFromDate
} from '../helpers/helpers';

globalThis.Timeline.Columns = function () {

    var _this = this;

    var { initialDate, viewport } = Timeline.Context;
    var { cellWidth } = Timeline.Config; 

    _this.container;                        // Root DOM element
    _this.elements = {};                    // Rendered columns
    _this.space = 0;                        // Current available space for columns
    _this.currentScale;

    _this.init = function () {
        _this.create();
        _this.render(initialDate);
    }
    
    _this.create = function () {
        
        // Calculate columns amount
        _this.space = Math.round(viewport.size.width / cellWidth);

        // Generate element from template and ensure container size
        _this.container = generateFromTemplate(
            Timeline.Templates.columnsTemplate.format(
                cellWidth * _this.space + cellWidth
        ))[0];

        viewport.container.appendChild(_this.container);
    }

    _this.recreate = function () {
        
        // Calculate columns amount
        _this.space = Math.round(viewport.size.width / cellWidth);

        // Set new size
        _this.container.style.width = `${cellWidth * _this.space + cellWidth}px`;
    }

    _this.clear = function () {

        // Clear DOM structure
        _this.container.innerHTML = "";

        // Clear internally stored data
        _this.elements = {};
    }
    
    _this.render = function (currentDate, recalculate) {
        const { scale } = Timeline.Context;

        if (recalculate) _this.recreate();

        // Render previous days
        for (var i = 0; i < _this.space / 2; i++) {
            var date = calculateNewDate(
                scale,
                currentDate,
                (Math.round(_this.space / 2) - i),
                1
            );
            _this.add(date, 0)
            
            if (i == 0) viewport.viewStartDate = date;
        }

        // Render upcomming days
        for (var i = 0; i <= Math.floor(_this.space / 2) + 1; i++) {
            var date = currentDate;
    
            if (i != 0) date = calculateNewDate(scale, currentDate, i, 0);
            _this.add(date, 0);
                        
            if (i == Math.floor(_this.space / 2))
                viewport.viewEndDate = date;
        }
    }

    // TODO: SHOW previous/current month on timeline
    _this.add = function (date, direction) {

        // Generate id based on current date
        var id = getIdFromDate(date);
        var column = _this.elements[id];
        
        if (!column) { 

            // Make calculations based on current scale
            var [timePart, classes, content] = getColumnContent(date);
            
            _this.elements[id] = generateFromTemplate(Timeline.Templates.columnTemplate.format(
                id,
                content,
                timePart,
                classes ? classes : '',
            ))[0];

            column = _this.elements[id];
        }

        // Insert column based on direction
        if (direction) _this.container.insertBefore(column, _this.container.children[0]);
        else _this.container.appendChild(column);
    }

    _this.update = function (direction) {
        const { scale } = Timeline.Context;

        // Get first and last column
        var firstColumn = _this.container.children[0];
        var lastColumn = _this.container.children[_this.container.children.length - 1];

        // Based on direction select which column in current
        var currentColumn = direction ? firstColumn : lastColumn;

        // Get current column date
        var columnDate = getDateFromId(currentColumn.id);

        // Calculate new column date
        columnDate = calculateNewDate(scale, columnDate, 1, direction);

        if (direction) {

            // Adjust current date
            Timeline.Context.currentDate = Timeline.Context.currentDate.addPeriod(-1);
            
            // Remove last column from view
            _this.container.removeChild(lastColumn);

            // Set new time period
            viewport.viewEndDate = getDateFromId(
                _this.container.children[
                    _this.container.children.length - 1
                ].id);
            viewport.viewStartDate = columnDate;
        } else {
            
            // Adjust current date
            Timeline.Context.currentDate = Timeline.Context.currentDate.addPeriod(1);

            // Remove first column from view
            _this.container.removeChild(firstColumn);

            // Set new time period
            viewport.viewStartDate = getDateFromId(_this.container.children[0].id);
            viewport.viewEndDate = columnDate;
        }

        // Add new column
        _this.add(columnDate, direction);
    }

    _this.init();
}