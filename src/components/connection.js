import { describePath, isNull } from '../helpers/helpers'

globalThis.Timeline.Connection = function (data) {

    var _this = this;

    var { viewport, coordinates } = Timeline.Context;
    var { onRelationDeleted } = Timeline.Context.events;

    _this.id = `conn${data.id}`;                  // Connection indifier
    _this.data = data;                   // Current data object
    _this.startNode;                     // Start item
    _this.endNode;                       // End item
    _this.removed;                       // If removed from DOM
    _this.group;                         // SVG group
    _this.path;                          // SVG path
    _this.hoverPath;                     // Path for better hover effect
    _this.pathData;                      // Current path data

    _this.init = function () {
        if (!_this.path) _this.create();

        // Reset flags
        _this.removed = true;

        // Ensure start and end nodes
        if (!_this.getNodes()) return;

        // Draw connection
        _this.update();
    }

    _this.create = function () {
        _this.group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        _this.group.setAttribute(
            "style",
            `transform: translateY(calc(0px - calc(var(--cellHeight) * var(--index))));`
        );

        _this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        _this.hoverPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        
        _this.path.setAttribute("id", _this.id);
        _this.hoverPath.setAttribute("id", `hover_${_this.id}`);
    
        _this.path.setAttribute(
            "style",
            `d: var(--conn${_this.id}path); `
        );
        _this.hoverPath.setAttribute(
            "style",
            `d: var(--conn${_this.id}path); `
        );

        _this.path.onclick = _this.handleClick;
        _this.hoverPath.onclick = _this.handleClick;

        _this.group.appendChild(_this.hoverPath);
        _this.group.appendChild(_this.path);
    }

    _this.handleClick = function (e) {
        _this.delete();
    }

    _this.delete = function () {
        onRelationDeleted({
            startNode: _this.startNode.data.id,
            endNode: _this.endNode.data.id
        });
    }

    _this.update = function (redraw) {
        if (!_this.getNodes()) return '';

        _this.ensure();

        if (redraw) {
            _this.pathData = `--conn${_this.id}path: path("${_this.describe()}");`;
            return _this.pathData;
        }
    }

    _this.getNodes = function () {

        // Load required informations
        var { startIndex, limit } = viewport.items;

        // Get start and end nodes
        _this.startNode = viewport.items.elements[_this.data.startNode];
        _this.endNode = viewport.items.elements[_this.data.endNode];

        function isParentExpanded(node) {
            
            if (!Timeline.Context.disableGroups &&
                !isNull(node.data.group) &&
                !Timeline.Context.viewport.items.expanded[`group${node.data.group}`])
                return false;

            // If no information about parents strucutre
            if (!node.data.parentTree || !node.data.parentTree.length) return true;

            return Timeline.Context.viewport.items.expanded[node.data.parent];
        }

        // If no start or end node
        if (!_this.startNode || !_this.endNode) return false;

        // If both nodes are removed from view
        if (_this.startNode.isRemoved && _this.endNode.isRemoved) return false;
        else if (!_this.startNode.startColumn || !_this.startNode.endColumn) return false;
        else if (!_this.endNode.startColumn || !_this.endNode.endColumn) return false;
        else if (_this.startNode.isRemoved && _this.startNode.index.index > startIndex && !_this.endNode.isRemoved) return false;
        else if (_this.endNode.isRemoved && _this.endNode.index.index > (startIndex + limit + 1) && !_this.startNode.isRemoved) return false;

        // If one of node is hidden ( parent not expanded )
        if (!isParentExpanded(_this.startNode) || !isParentExpanded(_this.endNode)) return false;

        return true;
    }

    _this.ensure = function () {
        
        // Load required informations
        var { startIndex, limit } = viewport.items;

        // Get indexes 
        var start = _this.startNode.index.index;
        var end = _this.endNode.index.index;
    
        // Check if indexes outside visibe space
        if ((start < startIndex && end < startIndex) ||
            (start > startIndex + limit && end > startIndex + limit)) {
                        
            if (_this.removed) return;
        
            // If yes remove connection from view
            viewport.connections.container.removeChild(_this.group);                    
        
            // Set deletion flag
            _this.removed = true;

            return false;
        } else if (_this.removed) {
            viewport.connections.container.appendChild(_this.group);

            // Set deletion flag
            _this.removed = false;

            return true;
        }           

        return true;
    }

    _this.describe = function () {
        var { cellHeight } = Timeline.Config;
        var { startNode, endNode } = _this;

        // Get start and end columns coordinates at X
        var columnStart = coordinates[startNode.endColumn.id];
        var columnEnd = coordinates[endNode.startColumn.id];

        // Check if start point is out of the viewport
        if (columnStart > viewport.size.width)
            columnStart = viewport.size.width + 100;
        else if (columnStart < 0)
            columnStart = -100;

        // Check if end point is out of the viewport
        if (columnEnd > viewport.size.width)
            columnEnd = viewport.size.width + 100;

        // Combine connection start point
        var startPoint = {
            x: columnStart,
            y: ((startNode.index.index) * cellHeight) + cellHeight / 2
        };

        // Combine connection end point
        var endPoint = {
            x: columnEnd,
            y: ((endNode.index.index) * cellHeight) + cellHeight / 2
        }

        // Return described connection
        return describePath(startPoint, endPoint, 20, cellHeight);
    }

    _this.onSelection = function () {
        console.log('connection clicked!');
    }
}