import {
    createSVGSpace,
    generateFromTemplate,
    createCurveTwoPoints,
    getStartPositionWithDirection,
    createConnectionStructure
} from '../helpers/helpers';

globalThis.Timeline.Connections = function () {

    var _this = this;

    var { viewport } = Timeline.Context;

    _this.container;                    // Root DOM element
    _this.elements = {};                // Rendered connections

    _this.direction;                    // New connection direction
    _this.startPosition;                // New connection direction
    _this.newConnection = {};           // New connection data

    _this.init = function () {
        _this.create();
        _this.render();
    }
    
    _this.create = function () {
        var container = generateFromTemplate(
            Timeline.Templates.connectionsTemplate)[0];

        /*viewport.container.insertBefore(
            container,
            viewport.items.container
        );*/

        viewport.container.appendChild(container);

        _this.container = createSVGSpace(container);

        // Dirty hack for drop event
        _this.container.ondrop = Timeline.Context.viewport.items.handleDrop;
    }

    _this.clear = function () {

        var marker = _this.container.children[0];

        // Clear current connections
        _this.container.innerHTML = '';

        // Add marker back
        _this.container.appendChild(marker);
    }

    _this.render = function () {
        const { connectionsData } = Timeline.Context;
        
        for (var i = 0; i < connectionsData.length; i++) {
            var connection = connectionsData[i];

            // TODO: refactor.. do not create all connections at once
            _this.add(connection);
        }

        viewport.update();
    }

    _this.add = function (data) {

        var connection = _this.elements[data.id];

        // If item not yet created...
        if (!connection) {
            _this.elements[data.id] = new Timeline.Connection(data);

            // Get connection object
            connection = _this.elements[data.id];

            // Initialize connnection
            connection.init();
        }
        else {
            connection.data = data;
            connection.removed = true;

            connection.update();
        }

        // Return newly created object
        return connection;
    }

    _this.update = function (redraw) {
        var result = '';
        
        Object.values(_this.elements).forEach(function (connection) {
            if (redraw)
                result += connection.update(redraw);
            else connection.update();
        });

        return result;
    }

    _this.new = function (args) {

        // Calculate interaction current position
        var x = args.default.clientX - viewport.size.offsetLeft;
        var y = args.default.clientY - viewport.size.parentTop - viewport.size.offsetTop - 75;

        // If called first time set new start position
        if (!_this.startPosition)
            [_this.direction, _this.startPosition] = getStartPositionWithDirection(args);

        // If connection container not exists yet
        if (!_this.newConnection.container) {
            _this.newConnection.container = createConnectionStructure(_this.startPosition);

            _this.container.classList.add('new');
        } 

        // Get new connection parts
        var currentPath = _this.newConnection.container.children[0];
        var currentCircle = _this.newConnection.container.children[1];

        // Draw line between new points
        createCurveTwoPoints(
            currentPath,
            _this.startPosition.x,
            _this.startPosition.y,
            x,
            y,
            _this.direction
        );
        
        // Change position of circle
        currentCircle.style.cx = x;
        currentCircle.style.cy = y;

        // Find currently hovered item
        var currentItem = viewport.items.container.children[
            Math.floor((y) / Timeline.Config.cellHeight)];

        // Remove hover effect from other items
        for(var i = 0; i < viewport.items.container.children.length; i++)
            viewport.items.container.children[i].classList.remove('hover');

        // If currently hovered item is current item
        if (currentItem.id == viewport.items.current.data.id ||
            currentItem.id.indexOf('group') != -1 ||
            currentItem.id.indexOf('dummy') != -1) {
            _this.newConnection.nodes = null;
            return;
        }

        // Make as hovered
        currentItem.classList.add('hover');

        // Save operation data based on direction
        if (_this.direction)
            _this.newConnection.nodes = {
                startNode: viewport.items.current.data.id,
                endNode: parseInt(currentItem.attributes["id"].value)
            };
        else
            _this.newConnection.nodes = {
                startNode: parseInt(currentItem.attributes["id"].value),
                endNode: viewport.items.current.data.id
            };
    }

    _this.finishNewConnection = function () {
        const { connectionsData } = Timeline.Context;
        var { items } = viewport;

        if (!items.current) return;

        // Check if connection contains required data
        if (_this.newConnection.nodes) {
            
            var newId = connectionsData && connectionsData.length ?
                connectionsData[connectionsData.length - 1].id + 1 : 0;

            // Create new connection object
            var connectionData = {
                id: newId,
                startNode: _this.newConnection.nodes.startNode,
                endNode: _this.newConnection.nodes.endNode
            };

            if (Timeline.Context.ensurePrevNext)
                Timeline.Context.viewport.items.ensureNextItemDates(
                    connectionData.startNode,
                    connectionData.endNode
                );

            // Push new data to source
            connectionsData.push(connectionData);

            // Add connection to current view
            _this.add(connectionData);

            // Fire global event
            Timeline.Context.events.onNewRelation(connectionData);
        }
        
        // Clear new connection styling
        _this.container.classList.remove('new');

        // Restore default node styling
        items.current.container.classList.remove('newConnection');

        // Remove hover effect from all other items
        for (var i = 0; i < items.container.children.length; i++) {
            items.container.children[i].classList.remove('hover');
        }

        // Remove new connection components from view
        _this.newConnection.container.remove();

        // Clear internal connection data
        _this.newConnection = {};
        _this.startPosition = null;
        _this.direction = null;

        // Recalculate view
        viewport.update();
    }
}