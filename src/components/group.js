import {
    generateFromTemplate, getColumnData, checkDatesDifference, getDatesRange
} from '../helpers/helpers';

globalThis.Timeline.Group = function (data) {

    var _this = this;

    var { cellHeight } = Timeline.Config;
    var { viewport } = Timeline.Context;
    var { onNewItem } = Timeline.Context.events;

    _this.data = data;                  // Data object
    _this.container;                    // Item DOM container
    _this.index;                        // Current item index
    _this.className;                    // Current cssClass holder

    _this.isRemoved = false;            // If item is removed from DOM
    _this.isExpanded = false;           // If item is expanded
    _this.isGroup = true;

    _this.cssClass = [];                // Item custom styles

    _this.init = function (index, recreate, direction, sibling) {
        if (!_this.container || recreate) _this.create();

        _this.isRemoved = false;
        _this.index = index;

        _this.append(direction, sibling);
    }
    
    _this.create = function () {
        if (!_this.data) return;

        _this.cssClass = [];

        // Check difference between dates ( how long it is )
        var isShort = checkDatesDifference(_this.data.startDate, _this.data.dueDate);

        var canEdit = _this.data.canEdit && _this.data.id.indexOf('external') == -1;
        var hasDates = true;

        // Add proper class if shape is short
        if (isShort) _this.cssClass.push('short');

        var startDate = _this.data.startDate ? _this.data.startDate : null;
        var dueDate = _this.data.dueDate ? _this.data.dueDate.addPeriod(1) : null;

        var startColumn = _this.data.startDate ? getColumnData(_this.data.startDate) : null;
        var endColumn = _this.data.dueDate ? getColumnData(_this.data.dueDate.addPeriod(1)) : null;

        var progress = 0;
        var progressMargin = 0;
        if (_this.data.progress) {

            var itemWidth = (getDatesRange(startDate, dueDate) * 50) - 10;
            progress = itemWidth * _this.data.progress;
            progressMargin = itemWidth - progress;

            _this.cssClass.push('progressView');

            if (_this.data.progress == 1) {
                _this.cssClass.push('completed');
                progressMargin = 0;
            }
        }

        // Select right group template
        if (Timeline.Context.showGroupsProgress && startColumn && endColumn) {

            // Generate item DOM
            _this.container = generateFromTemplate(
                Timeline.Templates.groupProgressTemplate.format(
                    _this.data.id,
                    _this.data.title,
                    _this.cssClass.join(' '),
                    _this.data.color,
                    startColumn ? startColumn.id : '',
                    endColumn ? endColumn.id : '',
                    progress,
                    progressMargin,
                    canEdit ? '<add></add>' : '',
                    isShort && startColumn ? `max-width: calc(10 * calc(18px + var(--${startColumn.id})));` : '',
                ))[0];
        } else {
            hasDates = false;

            // Generate item DOM
            _this.container = generateFromTemplate(
                Timeline.Templates.groupTemplate.format(
                    _this.data.id,
                    _this.data.title,
                    _this.cssClass.join(' '),
                    _this.data.color
                ))[0];
        }

        // Attach new item event
        if (canEdit)
            _this.container.children[0].children[0].children[0].onclick = _this.handleNewItem;

        // Attach expand event
        _this.container.children[0].children[2].onclick = _this.expand;

        if (hasDates) {
            _this.container.children[3].children[0].onclick = _this.expand;
        }
        else {
            _this.container.children[1].children[0].onclick = _this.expand;
        }

        if (_this.data.isExpanded) {
            _this.container.setAttribute('data-hasVisibleChildren', true);
            _this.isExpanded = true;
        }
    }

    _this.handleNewItem = function () {
        var container = _this.container.children[0].children[0].children[0];
        
        onNewItem({ id: _this.data.id.split('group')[1], target: container });
    }

    _this.remove = function () {

        // Set removed flag
        _this.isRemoved = true;

        // Remove from parent container
        viewport.items.container.removeChild(_this.container);

        // Adjust items counter
        Timeline.Context.itemsCount--;
    }

    // TODO: Refactor with writing XML structure
    _this.append = function (direction, sibling) {

        // If item should be inserted before any item
        if (sibling)
            // Add based on current direction
            if (!direction)
                viewport.items.container.insertBefore(
                    _this.container, sibling.nextSibling);
            else
                viewport.items.container.insertBefore(
                    _this.container, sibling);
        else
            // Add based on current direction
            if (!direction)
                viewport.items.container.appendChild(
                    _this.container);
            else
                viewport.items.container.insertBefore(
                    _this.container, viewport.items.container.children[0]);
    }

    _this.update = function (
        recreate,                   // If item need to be recreated
        args                        // Interaction args
    ) {

        if (recreate) {
            var old = _this.container;

            // Recreate item
            _this.create();

            if (!_this.removed)
                viewport.items.container.replaceChild(_this.container, old);

            viewport.update();
            
            return;
        }
    }

    _this.expand = function (args, inSet) {
        if (args && args.stopPropagation) args.stopPropagation();
        if (args && args.preventDefault) args.preventDefault();

        Object.keys(Timeline.Context.viewport.items.elements).forEach((e, i) => {
            Timeline.Context.viewport.items.elements[e].isRemoved = true;
        });

        if (_this.isExpanded) {
            _this.isExpanded = false;
            _this.container.setAttribute('data-hasVisibleChildren', false);
            delete viewport.items.expanded[_this.data.id];

            Timeline.Context.events.onClean({});
            Timeline.Context.events.onGroupExpanded({ id: _this.data.id.split('group')[1], isExpanded: _this.isExpanded });
            
            // Clear and rerender viewport ( recalculate )
            viewport.items.container.innerHTML = '';
            viewport.items.render();
        } else {
            // Mark as expanden
            _this.isExpanded = true;

            // Set right styling
            _this.container.setAttribute('data-hasVisibleChildren', true);

            // Set global information
            viewport.items.expanded[_this.data.id] = _this.data.id;

            Timeline.Context.events.onGroupExpanded({ id: _this.data.id.split('group')[1], isExpanded: _this.isExpanded });
    
            // Clear and rerender viewport ( recalculate )
            viewport.items.container.innerHTML = '';
            viewport.items.render();
        }

        viewport.items.ensureMasksPosition();
        viewport.update();
    }
}