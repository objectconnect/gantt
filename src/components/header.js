import { generateFromTemplate } from '../helpers/helpers';

globalThis.Timeline.Header = function (context) {
    var _this = this;

    _this.header;

    _this.floatingBox = null;

    _this.movementInstance;                         // This movement helper instance

    _this.createHeader = function () {
        _this.header = generateFromTemplate(Timeline.Templates.headerTemplate)[0];
        context.rootContainer.appendChild(_this.header);
    }

    _this.init = function () {
        _this.createHeader();    

        _this.header.children[0].children[0].onclick = function () { _this.showDialogBox(0) };
        _this.header.children[1].children[0].onclick = function () { _this.showDialogBox(1) };
        _this.header.children[2].children[0].onclick = function () { _this.showDialogBox(1) };

        for (var x = 0; x < _this.header.children[3].children.length; x++) {
            _this.header.children[3].children[x].onclick = function (e) { _this.changeCalendar(e); }
        }
    }

    _this.hasDrag = false;
    _this.draggedElement;
    _this.dragSource;
    _this.differenceLeft = 0;
    _this.differenceTop = 0;
    _this.onMovementStarted = function (args) {
        if (args.target.className == 'emptyNode') {
            _this.hasDrag = true;
            _this.dragSource = args.target;
            _this.draggedElement = _this.dragSource.cloneNode(true);
            
            _this.draggedElement.className = 'draggedEmptyNode';
            _this.dragSource.classList.add('dragged');
            
            var rect = _this.dragSource.getBoundingClientRect();
            
            _this.differenceLeft = args.clientX - rect.left;
            _this.differenceTop = args.clientY - rect.top;
            
            _this.draggedElement.style.left = (args.clientX - _this.differenceLeft) + 'px';
            _this.draggedElement.style.top = (args.clientY - _this.differenceTop) + 'px';

            Timeline.Context.rootContainer.appendChild(_this.draggedElement);
            Timeline.Context.viewport.items.handleDragStart(args.target.attributes['data-id'].value);
        }
    }

    _this.onMovement = function (args) {
        if (_this.hasDrag) {
            _this.draggedElement.style.left = (args.mouse.x - _this.differenceLeft) + 'px';
            _this.draggedElement.style.top = (args.mouse.y - _this.differenceTop) + 'px';

            Timeline.Context.viewport.items.handleDragOver(args.mouse.x);
        }
    }

    _this.onMovementFinished = function (args) {
        if (!_this.hasDrag || !_this.draggedElement) return;

        _this.draggedElement.remove();

        if (!Timeline.Context.viewport.items.handleDrop()) {
            _this.dragSource.classList.remove('dragged');
        }

        _this.hasDrag = false;
        _this.dragSource = null;
        _this.draggedElement = null;
        _this.differenceLeft = 0;
        _this.differenceTop = 0;
    }

    _this.changeCalendar = function (args, scroll = false, scale) {

        for (var x = 0; x < _this.header.children[3].children.length; x++)
            _this.header.children[3].children[x].classList.remove('selected');

        if (scroll) {
            var direction;
            
            if (args.deltaY > 0)
                direction = 0;

            if (args.deltaY < 0)
                direction = 1;

            context.scale = direction ? context.scale + 1 : context.scale - 1;
            
            if (context.scale < 0) context.scale = 0;
            if (context.scale > (_this.header.children[3].children.length - 2))
                context.scale = _this.header.children[3].children.length - 2;

            _this.header.children[3].children[context.scale].classList.add('selected');
        } else if (args) {
            args.target.classList.add('selected');
            context.scale = parseInt(args.target.attributes['data-scale'].value);
        } else {
            context.scale = scale;
            _this.header.children[3].children[context.scale].classList.add('selected');
        }
        
        // Clear stored dates
        Timeline.Context.dates = {};

        Timeline.Context.refresh();

        if (args) Timeline.Context.events.onViewChanged({ 'scale': context.scale });
    }

    _this.showDialogBox = function (contentType) {
        if (_this.floatingBox) {
            var isThisSame = contentType.toString() == _this.floatingBox.id;
            _this.floatingBox.style.cssText = 'right: -300px;';

            setTimeout(function () {
                if (!_this.floatingBox) return;

                _this.floatingBox.remove();
                _this.floatingBox = null;
            }, 500);

            if (isThisSame) return;
        }

        var title = '';
        var content = '';

        switch (contentType) {
            case 0:
                title = "Group items";
                content = Timeline.Templates.groupPanelTemplate;
                break;
            case 1:
                title = "Items without dates";

                Object.keys(Timeline.Context.emptyNodes).forEach((e, i) => {
                    var item = Timeline.Context.emptyNodes[e];

                    if (!item) return;

                    content += Timeline.Templates.emptyNodeTemplate.format(item.id, item.title);
                });

                content = Timeline.Templates.emptyNodesListTemplate.format(content);

                break;
        }

        _this.floatingBox = generateFromTemplate(
            Timeline.Templates.floatingBoxTemplate.format(contentType, title, content))[0];
        _this.header.appendChild(_this.floatingBox);
        
        _this.floatingBox.children[1].onclick = function () { _this.showDialogBox(_this.floatingBox.id); };

        // Initialize Movement helper
        _this.movementInstance = new Timeline.MovementHelper(
            _this.floatingBox,
            null,
            _this.onMovementStarted,
            _this.onMovement,
            _this.onMovementFinished,
            false,
            null
        );
        _this.movementInstance.init();

        /*_this.floatingBox.querySelectorAll('.emptyNode').forEach(item => {
            item.ondragstart = function (event) {
              var currentId = event.target.attributes['data-id'].value;
              event.dataTransfer.setData("id", currentId);
            };
        });*/

        setTimeout(function () { _this.floatingBox.style.cssText = 'right: 0px;'; }, 1);
    }
    
    _this.init();
}