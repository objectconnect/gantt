import { UserActionEnum, TIMELINESCALE } from '../constants/enums';
import {
    getIdFromDate,
    getColumnData,
    generateFromTemplate,
    getDateFromId,
    getDatesRange,
    isNull,
    renderFieldValue,
    checkDatesDifference,
    ensureDateByScale
} from '../helpers/helpers';

globalThis.Timeline.Item = function (data) {

    var _this = this;

    var { cellHeight } = Timeline.Config;
    var { viewport } = Timeline.Context;
    var {
        onItemClick,
        onItemRender,
        onItemDestroy,
        onItemMoved,
        onItemStartDateChanged,
        onItemDueDateChanged,
        onValueRender
    } = Timeline.Context.events;

    _this.data = data;                  // Data object
    _this.title;
    _this.container;                    // Item DOM container
    _this.element;                      // Visible DOM element
    _this.index;                        // Current item index
    _this.className;                    // Current cssClass holder
    _this.startColumn = {};             // Item start column
    _this.endColumn = {};               // Item end column
    _this.indLeft = {};                 // Children indicator left
    _this.indRight = {};                // Children indicator left
    _this.nextSiblingId;                // Next item in view when item is collapsed
    _this.canEdit = false;

    _this.isHovered = false;            // Hovered style by moving and resizing
    _this.isRemoved = false;            // If item is removed from DOM
    _this.isExpanded = false;           // If item is expanded
    _this.hasChildren = false;          // If item has children
    _this.noDates = false;              // If item has no dates

    _this.renderedParts = {};

    _this.itemRendered = false;

    _this.cssClass = [];                // Item custom styles
    _this.externalFields = [];

    _this.init = function (index, recreate, direction, sibling) {
        if (!_this.container) _this.create();
        if (recreate) _this.updateProps(true);

        _this.isRemoved = false;
        _this.index = index;

        _this.append(direction, sibling);

        if (Timeline.Context.showUserAvatar) { //&& !_this.renderedParts['user']) {
            _this.renderedParts['user'] = _this.container.getElementsByTagName('user')[1];

            if (_this.renderedParts['user'])
                onItemRender({ id: _this.data.id, container: _this.renderedParts['user'] });
        }

        if (_this.externalFields.length) _this.externalFields.forEach((e, i) => {

            // TODO: add info about value
            // if (_this.renderedParts[e.containerId]) return;

            _this.renderedParts[e.containerId] = document.getElementById(e.containerId);
            onValueRender({ id: _this.data.id, column: e.source, container: _this.renderedParts[e.containerId], value: e.value })
        });
    }

    _this.validateDates = function (adjustDueDate) {
        var proxyStartDate = _this.data.startDate ? _this.data.startDate : _this.data.dueDate; 
        var proxyEndDate = (_this.data.dueDate ? 
            (adjustDueDate ? _this.data.dueDate.addPeriod(1) : _this.data.dueDate) :
            (adjustDueDate ? _this.data.startDate.addPeriod(1) : _this.data.startDate));

        if (proxyStartDate.diff(proxyEndDate, 'days') > 0)
            proxyStartDate = _this.data.dueDate.addPeriod(0);

        return [proxyStartDate, proxyEndDate];
    }
    
    _this.create = function () {
        if (!_this.data) return;
        if (!_this.data.startDate && !_this.data.dueDate) return _this.createEmpty();

        _this.cssClass = [];

        _this.canEdit = _this.data.canEdit;

        if (!_this.canEdit)
            _this.cssClass.push('noperm');

        // Item header structure
        var itemHeader = '';

        if (!_this.data.startDate) _this.data.startDate = _this.data.dueDate;
        if (!_this.data.dueDate) _this.data.dueDate = _this.data.startDate;

        var [proxyStartDate, proxyEndDate] = _this.validateDates(true);

        // Load columns information
        _this.startColumn = getColumnData(proxyStartDate);
        _this.endColumn = getColumnData(proxyEndDate);

        // Check item status ( custom styles )
        if (_this.data.issueLevel) _this.checkStatus();

        // Check difference between dates ( how long it is )
        var isShort = checkDatesDifference(_this.data.startDate, _this.data.dueDate);

        // Add proper class if shape is short
        if (isShort) _this.cssClass.push('short');
        if (_this.isHovered) _this.cssClass.push('hover');
        if (!Timeline.Context.showUserAvatar) _this.cssClass.push('hideUser');

        // Title is default
        itemHeader = `<span>${_this.data.title}</span>`;
        _this.title = _this.data.title;

        // Move to new function
        _this.externalFields = [];
        // Check what columns to display ( title is default )
        if (Timeline.Context.viewColumns.length) {
            Timeline.Context.viewColumns.forEach(function (c, i) {
                if (c.source)
                    itemHeader += renderFieldValue(c, Timeline.Context.sources[c.source][_this.data[c.internalName]]);
                else {
                    var containerId = `${_this.data.id}_viewColumn_${c.internalName}`;

                    if (_this.renderedParts[containerId]) return;
                    
                    var cssClass = '';
                    if (c.internalName == 'responsible') cssClass = 'class="responsible"';
                    else if (c.internalName == 'labels') cssClass = 'class="labels"';

                    itemHeader += `<span id='${containerId}' ${cssClass}></span>`;

                    if (isNull(_this.data[c.internalName])) return;

                    _this.externalFields.push({ value: _this.data[c.internalName], containerId: containerId, source: c.internalName });
                }
            });
        }

        if (_this.data.parentTree && _this.data.parentTree.length)
            _this.cssClass.push(`level${(_this.data.parentTree.length + (Timeline.Context.hasGroups ? 1 : 0))}`);
        else if (Timeline.Context.hasGroups && !isNull(_this.data.group)) _this.cssClass.push('level1');

        // Check current card progress
        var progress = 0;
        var progressMargin = 0;
        if (_this.data.progress) {

            var itemWidth = (getDatesRange(proxyStartDate, proxyEndDate) * 50) - 10;
            progress = itemWidth * (_this.data.progress / 100);
            progressMargin = itemWidth - progress;

            _this.cssClass.push('progressView');

            if (_this.data.isCompleted) {
                _this.cssClass.push('completed');   
            }
        }

        // Generate item DOM
        _this.container = generateFromTemplate(
            Timeline.Templates.itemTemplate.format(
                _this.data.id,
                _this.data.title,
                _this.startColumn.id,
                _this.endColumn.id,
                _this.cssClass.join(' '),
                isShort ? `max-width: calc(10 * calc(18px + var(--${_this.startColumn.id})));` : '',
                itemHeader,
                progress,
                progressMargin
            ))[0];

        // Bind shape to object
        _this.element = _this.container.children[3];

        // Attach item events
        _this.element.onclick = _this.handleItemClick;
        _this.container.children[0].children[0].onclick = _this.handleItemClick;

        // Attach expand event
        _this.container.children[0].children[2].onclick = _this.expand;
        _this.element.children[2].children[0].onclick = _this.expand;

        _this.checkIfParent();
        _this.ensureMask();
    }

    _this.handleItemClick = function (e) {

        // Prevent item click event for users
        /* if (Timeline.Context.dynamicInteraction ||
            e.target.className.indexOf('Avatar') != -1) return; */

        onItemClick(_this.data.id);
    }

    _this.createEmpty = function () {

        if (_this.data) _this.canEdit = _this.data.canEdit;

        // Title is default
        var itemHeader = `<span>${_this.data.title}</span>`;
        _this.title = _this.data.title;

        // Move to new function
        _this.externalFields = [];
        // Check what columns to display ( title is default )
        if (Timeline.Context.viewColumns.length) {
            Timeline.Context.viewColumns.forEach(function (c, i) {
                if (c.source)
                    itemHeader += renderFieldValue(c, Timeline.Context.sources[c.source][_this.data[c.internalName]]);
                else {
                    var containerId = `${_this.data.id}_viewColumn_${c.internalName}`;

                    if (_this.renderedParts[containerId]) return;
                    
                    var cssClass = '';
                    if (c.internalName == 'responsible') cssClass = 'class="responsible"';
                    else if (c.internalName == 'labels') cssClass = 'class="labels"';

                    itemHeader += `<span id='${containerId}' ${cssClass}></span>`;

                    if (isNull(_this.data[c.internalName])) return;

                    _this.externalFields.push({ value: _this.data[c.internalName], containerId: containerId, source: c.internalName });
                }
            });
        }

        // Generate empty item DOM
        _this.container = generateFromTemplate(
            Timeline.Templates.emptyItemTemplate.format(_this.data.id, itemHeader))[0];

        _this.container.children[0].children[0].onclick = _this.handleItemClick;
        // TODO: Do this only if it is parent
        _this.container.children[0].children[2].onclick = _this.expand;

        _this.container.children[1].onclick = _this.handleDateSelection;
        _this.noDates = true;

        if (_this.data.parentTree && _this.data.parentTree.length)
            _this.cssClass.push(`level${(_this.data.parentTree.length + (Timeline.Context.hasGroups ? 1 : 0))}`);
        else if (Timeline.Context.hasGroups && !isNull(_this.data.group)) _this.cssClass.push('level1');

        _this.container.className = _this.cssClass.join(' ');

        _this.checkIfParent();
    }

    _this.remove = function () {

        // Fire global event
        onItemDestroy({ id: _this.data.id });

        // Set removed flag
        _this.isRemoved = true;

        // Remove from parent container
        viewport.items.container.removeChild(_this.container);

        // Adjust items counter
        Timeline.Context.itemsCount--;
    }

    // TODO: Refactor with writing XML structure
    _this.append = function (direction, sibling) {

        // If item should be inserted before any item
        if (sibling)
            // Add based on current direction
            if (!direction)
                viewport.items.container.insertBefore(
                    _this.container, sibling.nextSibling);
            else
                viewport.items.container.insertBefore(
                    _this.container, sibling);
        else
            // Add based on current direction
            if (!direction)
                viewport.items.container.appendChild(
                    _this.container);
            else
                viewport.items.container.insertBefore(
                    _this.container, viewport.items.container.children[0]);
    }

    _this.update = function (
        recreate,                   // If item need to be recreated
        hasNewStartDate,            // If item has new start date
        hasNewEndDate,              // If item has new end date
        args                        // Interaction args
    ) {
        var needViewportUpdate = false;

        if (hasNewStartDate || hasNewEndDate) {
            var [startDate, dueDate] = _this.validateDates();

            if (hasNewStartDate) {
                //var startDate = _this.data.startDate ? _this.data.startDate : _this.data.dueDate;

                _this.data.startDate = ensureDateByScale(startDate.addPeriod(
                    args.direction.x ? -args.change.x : args.change.x), 'startOf');


                if (viewport.viewStartDate.valueOf() > _this.data.startDate.valueOf())
                    needViewportUpdate = true;
            }

            if (hasNewEndDate) {
                //var dueDate = _this.data.dueDate ? _this.data.dueDate : _this.data.startDate;

                _this.data.dueDate = ensureDateByScale(dueDate.addPeriod(
                    args.direction.x ? -args.change.x : args.change.x), 'endOf');

                if (viewport.viewEndDate.addPeriod(-1).valueOf() < _this.data.dueDate.valueOf())
                    needViewportUpdate = true;
            }
        }
        
        if (needViewportUpdate) {

            // Prevent vertical movement
            args.change.y = 0;

            // Direction must be oposite to current user mouse move
            args.direction.x = !args.direction.x;

            // Trigger viewport update
            viewport.handleViewportMovement(args);

        }

        if (recreate) {
            _this.updateProps();
            
            return;
        }
        
        if (!_this.data || (!_this.data.startDate && !_this.data.dueDate)) return;

        // Set new item columns
        _this.startColumn = getColumnData(_this.data.startDate ? _this.data.startDate : _this.data.dueDate);
        _this.endColumn = getColumnData(_this.data.dueDate ? _this.data.dueDate : _this.data.startDate);
    }

    _this.updateProps = function (complete) {

        if (!_this.data.startDate && !_this.data.dueDate) {
            _this.create();
            return;
        }

        _this.canEdit = _this.data.canEdit;

        _this.cssClass = [];

        if (!_this.canEdit)
            _this.cssClass.push('noperm');

        if (complete) {
            if (!_this.data.startDate) _this.data.startDate = _this.data.dueDate;
            if (!_this.data.dueDate) _this.data.dueDate = _this.data.startDate;
        }

        var proxyStartDate = _this.data.startDate ? _this.data.startDate : _this.data.dueDate; 
        var proxyEndDate = _this.data.dueDate ? _this.data.dueDate.addPeriod(1) : _this.data.startDate.addPeriod(1);

        if (proxyStartDate.diff(proxyEndDate, 'days') > 0)
            proxyStartDate = _this.data.dueDate.addPeriod(0); 

        // Load columns information
        _this.startColumn = getColumnData(proxyStartDate);
        _this.endColumn = getColumnData(proxyEndDate);

        if (_this.data.title != _this.title) {
            _this.title = _this.data.title;
            _this.element.children[2].children[1].innerText = _this.title;
            _this.container.children[0].children[0].children[0].innerText = _this.title;
        }

        // Check current card progress
        var progress = 0;
        var progressMargin = 0;
        if (_this.data.progress) {

            var itemWidth = (getDatesRange(proxyStartDate, proxyEndDate) * 50) - 10;

            progress = itemWidth * (_this.data.progress / 100);
            progressMargin = itemWidth - progress;

            _this.cssClass.push('progressView');

            if (_this.data.isCompleted) {
                _this.cssClass.push('completed');
                //progressMargin = 0;
            }
        }

        _this.container.style.cssText = `--left: var(--${_this.startColumn.id}); --right: var(--${_this.endColumn.id}); --progress: ${progress}px; --progressMargin: ${progressMargin}px;`;

        // Check item status ( custom styles )
        if (_this.data.issueLevel) _this.checkStatus();

        // Check difference between dates ( how long it is )
        var isShort = checkDatesDifference(_this.data.startDate, _this.data.dueDate);
        // Add proper class if shape is short
        if (isShort && _this.element) {
            _this.cssClass.push('short');
            _this.element.children[2].style = `max-width: calc(10 * calc(18px + var(--${_this.startColumn.id})));`;
        }

        if (_this.isHovered) _this.cssClass.push('hover');
        if (!Timeline.Context.showUserAvatar) _this.cssClass.push('hideUser');

        if (_this.data.parentTree && _this.data.parentTree.length)
            _this.cssClass.push(`level${(_this.data.parentTree.length + (Timeline.Context.hasGroups ? 1 : 0))}`);
        else if (Timeline.Context.hasGroups && !isNull(_this.data.group)) _this.cssClass.push('level1');

        _this.container.className = _this.cssClass.join(' ');

        _this.checkIfParent();
        _this.ensureMask();

        viewport.update();
    }

    _this.checkStatus = function () {

        // Check current item status and set right title
        if (_this.data.issueLevel == 'danger') _this.cssClass.push('danger');
        else if (_this.data.issueLevel == 'warning') _this.cssClass.push('warning');
    }

    _this.checkIfParent = function () {
        
        // If item have any child
        if (!_this.data.children || !_this.data.children.length) {
            if (_this.hasChildren) {
                _this.hasChildren = false;
                _this.container.setAttribute("data-hasChildren", false);

                if (_this.indLeft.object) {
                    _this.indLeft.object.remove();
                    _this.indLeft = {}; 
                }

                if (_this.indRight.object) {
                    _this.indRight.object.remove();
                    _this.indRight = {};
                }
            }

            return;
        };

        var hasValidItems = false;
        _this.data.children.forEach((child, i) => {
            if (!Timeline.Context.emptyNodes[child] && Timeline.Context.indexes[child]) {
                hasValidItems = true;
                return;
            }
        });

        if (!hasValidItems) {
            _this.container.setAttribute("data-hasChildren", false);
            _this.hasChildren = false;
            return;
        }

        // Set item indicator for children
        _this.hasChildren = true;

        // Set DOM object children indicators
        _this.container.setAttribute("data-hasChildren", true);
        _this.container.setAttribute('data-hasVisibleChildren', _this.isExpanded);

        if (_this.data.childrenStartDate &&
            _this.data.startDate.valueOf() > _this.data.childrenStartDate.valueOf()) {
        
            // Get indicator columnId
            _this.indLeft.id = getIdFromDate(_this.data.childrenStartDate);

            // Save info about requested date
            Timeline.Context.dates[_this.indLeft.id] = _this.data.childrenStartDate;

            if (!_this.indLeft.object) {
                _this.indLeft.object = document.createElement('indicatorLeft');
                _this.element.appendChild(_this.indLeft.object);
            }

            _this.indLeft.object.style.cssText =
                `left: calc(calc(var(--${_this.indLeft.id}) - var(--${_this.startColumn.id})) - 2px);`;
        } else if (_this.indLeft.object) {
            _this.indLeft.object.remove();
            _this.indLeft = {}; 
        }

        if (_this.data.childrenEndDate &&
            _this.data.dueDate.valueOf() < _this.data.childrenEndDate.valueOf()) {
        
            // Get indicator columnId
            _this.indRight.id = getIdFromDate(_this.data.childrenEndDate.addPeriod(1));

            // Save info about requested date
            Timeline.Context.dates[_this.indRight.id] = _this.data.childrenEndDate;

            if (!_this.indRight.object) {
                _this.indRight.object = document.createElement('indicatorRight');
                _this.element.appendChild(_this.indRight.object);
            }

            _this.indRight.object.style.cssText =
                `width: calc(var(--${_this.indRight.id}) - var(--${_this.endColumn.id}));` +
                `left: calc(calc(var(--${_this.endColumn.id}) - var(--${_this.startColumn.id})) - 10px);`
        } else if (_this.indRight.object) {
            _this.indRight.object.remove();
            _this.indRight = {};
        }
    }

    _this.expand = function (args, inSet) {
        if (args && args.stopPropagation) args.stopPropagation();
        if (args && args.preventDefault) args.preventDefault();

        if (_this.isExpanded) {
            _this.isExpanded = false;
            _this.container.setAttribute('data-hasVisibleChildren', false);
            delete viewport.items.expanded[_this.data.id];

            viewport.items.masks[_this.data.id].element.remove();
            delete viewport.items.masks[_this.data.id];

            if (_this.data.children) _this.data.children.forEach((c, i) => {
                if (viewport.items.expanded[c])
                    viewport.items.elements[c].expand(null, true);
            });

            if (inSet != true) {
                viewport.items.container.innerHTML = '';
                viewport.items.render();
            }

            if (inSet != true && !isNull(_this.data.parent) && Timeline.Context.indexes[_this.data.parent])
                viewport.items.updateMaskTree(_this.data.parent);
        } else {
            var atEnd;

            // Find next element
            _this.nextSiblingId = viewport.items.findNext(_this.index.dataIndex);

            // Mark as expanden
            _this.isExpanded = true;

            // Set right styling
            _this.container.setAttribute('data-hasVisibleChildren', true);

            // Set global information
            viewport.items.expanded[_this.data.id] = _this.data.children.length;
    
            // Clear and rerender viewport ( recalculate )
            viewport.items.container.innerHTML = '';
            viewport.items.render();

            if (isNull(_this.nextSiblingId)) {
                _this.nextSiblingId = viewport.items.lastIndex ? viewport.items.lastIndex.id : null;
                
                atEnd = true;
            } else _this.nextSiblingId = _this.nextSiblingId.id;

            var maskHeight = 0;
            if (atEnd && isNull(_this.nextSiblingId)) {
                maskHeight = (_this.data.children.length * cellHeight) + cellHeight;
            } else {
                var nextIndex = viewport.items.elements[_this.nextSiblingId].index.index;
                maskHeight = ((nextIndex - _this.index.index) * cellHeight) + (atEnd ? cellHeight : 0);
            }
        
            var left = _this.indLeft.object ? _this.indLeft.id : _this.startColumn.id;
            var right = _this.indRight.object ? _this.indRight.id : _this.endColumn.id;

            var mask = document.createElement('mask');
            mask.style.cssText =
                (_this.data.hasError && !_this.data.ignoreDateConflicts ? `background-color: red; ` : '') +
                `left: var(--${left}); ` +
                `right: calc(calc(100% - var(--${right}))); ` +
                `top: calc(calc(0px - calc(var(--cellHeight) * var(--index))) + ${_this.index.index * cellHeight}px); ` +
                `height: ${maskHeight}px; `;
            
            viewport.items.masks[_this.data.id] = {
                index: _this.index,
                element: mask,
                parent: _this.data.parent
            };

            if (!isNull(_this.data.parent) && Timeline.Context.indexes[_this.data.parent]) viewport.items.updateMaskTree(_this.data.parent);
            viewport.items.underground.appendChild(mask);
        }

        viewport.items.ensureMasksPosition();
    }

    _this.ensureMask = function (top) {

        if (!viewport.items.masks[_this.data.id]) return;
        if (!_this.hasChildren && _this.isExpanded) {
            _this.expand();
            return;
        }
        
        var mask = viewport.items.masks[_this.data.id].element;

        if (_this.data.hasError && !_this.data.ignoreDateConflicts)
            mask.style.backgroundColor = 'red';
        else mask.style.backgroundColor = null;

        var left = _this.indLeft.object ? _this.indLeft.id : _this.startColumn.id;
        var right = _this.indRight.object ? _this.indRight.id : _this.endColumn.id;

        mask.style.left = `var(--${left})`;
        mask.style.right = `calc(calc(100% - var(--${right})))`;

        if (top) mask.style.top = `calc(calc(0px - calc(var(--cellHeight) * var(--index))) + ${_this.index.index * cellHeight}px)`
    }

    _this.resize = function (args) {
        
        if (!_this.canEdit) return;

        // Set hover effect on item
        _this.isHovered = true;
        
        // Find interaction direction
        var side = viewport.startElement == 'RESIZERIGHT';

        // Update current item
        _this.update(true, !side, side, args);
    }

    _this.move = function (args) {
        
        if (!_this.canEdit) return;

        // Set hover effect on item
        _this.isHovered = true;

        _this.update(true, true, true, args);
    }

    _this.handleDateSelection = function (args) {

        if (!_this.canEdit) return;
 
        viewport.handleColumnSelection({
            mouse: {
                x: args.clientX,
                y: args.clientY
            }
        });

        if (!viewport.startElement) return;

        viewport.startElement.classList.remove('hover');

        var selectedDate = getDateFromId(viewport.startElement.id);
        _this.data.startDate = ensureDateByScale(selectedDate.clone(), 'startOf').set({'hour': 9, 'minute': 0, 'second': 0});
        _this.data.dueDate = ensureDateByScale(selectedDate.clone(), 'endOf').set({'hour': 23, 'minute': 0, 'second': 0});
        
        delete viewport.items.elements[_this.data.id];

        onItemMoved({
            id: _this.data.id,
            startDate: _this.data.startDate.toDate(),
            dueDate: _this.data.dueDate.toDate()
        });

        viewport.startElement.classList.remove('hover');
        viewport.startElement = null;
    }

    _this.finishAction = function (action) {

        // Timeline.Context.dynamicInteraction = false;

        if (!_this.canEdit) return;

        var removed = false;
        if (action == UserActionEnum.RESIZE_NODE &&
            _this.data.startDate && _this.data.dueDate && 
            _this.data.startDate.diff(_this.data.dueDate) > 0) {
            _this.data.startDate = null;
            _this.data.dueDate = null;

            _this.startColumn = null;
            _this.endColumn = null;

            removed = true;
            _this.isRemoved = true;

            viewport.connections.clear();
            viewport.update();

            onItemMoved({
                id: _this.data.id,
                startDate: null,
                dueDate: null,
                test: 'test'
            });
        }
        
        if (!removed && Timeline.Context.skipWeekends) {
            if (_this.data.startDate && (_this.data.startDate.format('dddd') == 'Saturday' || _this.data.startDate.format('dddd') == 'Sunday'))
                if (_this.data.startDate.format('dddd') == 'Saturday')
                    _this.data.startDate = _this.data.startDate.add(2, 'days');
                else if (_this.data.startDate.format('dddd') == 'Sunday')
                    _this.data.startDate = _this.data.startDate.add(1, 'days');

            if (_this.data.dueDate && (_this.data.dueDate.format('dddd') == 'Saturday' || _this.data.dueDate.format('dddd') == 'Sunday'))
                if (Timeline.Context.scale == TIMELINESCALE.DAY)
                    if (_this.data.dueDate.format('dddd') == 'Saturday')
                        _this.data.dueDate = _this.data.dueDate.add(2, 'days');
                    else if (_this.data.dueDate.format('dddd') == 'Sunday')
                        _this.data.dueDate = _this.data.dueDate.add(1, 'days');
                else
                    if (_this.data.dueDate.format('dddd') == 'Saturday')
                        _this.data.dueDate = _this.data.dueDate.add(-1, 'days');
                    else if (_this.data.dueDate.format('dddd') == 'Sunday')
                        _this.data.dueDate = _this.data.dueDate.add(-2, 'days');
        }

        if (!removed) {
            _this.data.startDate.set({'hour': 9, 'minute': 0, 'second': 0})
            _this.data.dueDate.set({'hour': 23, 'minute': 0, 'second': 0})
        }

        if (!removed)
            switch (action) {
                case UserActionEnum.RESIZE_NODE:
                    // Find interaction direction
                    var side = viewport.startElement == 'RESIZERIGHT';

                    // Dispatch public event
                    if (side)
                        onItemDueDateChanged({
                            id: _this.data.id,
                            dueDate: _this.data.dueDate.toDate()
                        });
                    else
                        onItemStartDateChanged({
                            id: _this.data.id,
                            startDate: _this.data.startDate.toDate()
                        });

                    break;

                case UserActionEnum.MOVE_NODE:
                    // Dispatch public event
                    onItemMoved({
                        id: _this.data.id,
                        startDate: _this.data.startDate.toDate(),
                        dueDate: _this.data.dueDate.toDate()
                    });
                    break;
            }

        viewport.items.ensureMasksPosition();

        // Prevent flickering by incoming update after user interaction
        setTimeout(function () {            
            var index = _this.cssClass.indexOf('hover');

            if (index != -1) {
                _this.cssClass.splice(index -1, 1);
                _this.isHovered = false;
                _this.container.classList.remove('hover');
            }
        }, 250);
    }

    _this.finishDateSelection = function (column) {
        if (!column) return;

        // Clear column styles
        column.classList.remove('hover');

        // Set item dates
        _this.data.startDate = getDateFromId(column.id);
        _this.data.dueDate = _this.data.startDate.addPeriod(1);

        // Exit column selection
        viewport.items.byColumnSelection = false;

        // Update item
        _this.update(true);
    }
}