import { TIMELINESCALE } from '../constants/enums';
import {
    generateFromTemplate, getDateFromId, isNull, ensureDateByScale
} from '../helpers/helpers';

globalThis.Timeline.Items = function () {

    var _this = this;

    const { viewport } = Timeline.Context;

    _this.container;                        // Root DOM element
    _this.underground;                      // Additional objects container
    _this.elements = {};                    // Rendered items
    _this.expanded = {};                    // Currently expanden items ( or childrens or groups )
    _this.masks = {};                       // Parents masks ( or other elements based on items ( additional elements ) )

    _this.firstIndex;                       // First index visible in view
    _this.lastIndex;                        // Last index visible in view
    _this.startIndex = 0;                   //
    _this.limit = 0;                        // Max items in view

    _this.current;                          // Currently processed item

    _this.groups;                           // Groups
    _this.sorting;                          // Sorting
    _this.searchQuery;                      // Applied search query

    _this.draggedItem;                      // Currently dragged item
    _this.byColumnSelection = false;        // If setting new dates

    Timeline.Context.itemsCount = 0;        // Currently visible items

    _this.init = function () {
        _this.create();
        _this.render();
    }

    _this.create = function () {
        _this.underground = generateFromTemplate(Timeline.Templates.masksTemplate)[0];
        _this.container = generateFromTemplate(Timeline.Templates.itemsTemplate)[0];

        viewport.container.appendChild(_this.underground);
        viewport.container.appendChild(_this.container);

        viewport.container.ondragover = _this.handleDragOver;

        document.documentElement.style.setProperty('--index', _this.startIndex);
    }

    _this.handleDragStart = function (id) {
        viewport.items.byColumnSelection = true;
        _this.draggedItem = id;
    }

    _this.handleDragOver = function (x) {
        x.preventDefault();
        x.stopPropagation();

        // Timeline.Context.dynamicInteraction = true;
        viewport.handleColumnSelection({ mouse: { x: x.x }});
    }

    _this.handleDrop = function (e) {
        e.preventDefault();
        e.stopPropagation();

        var destinationColumn = viewport.startElement;

        if (!destinationColumn) return false;

        var startDate = ensureDateByScale(getDateFromId(destinationColumn.id), 'startOf').set({'hour': 9, 'minute': 0, 'second': 0}).toDate();
        var dueDate = ensureDateByScale(getDateFromId(destinationColumn.id), 'endOf').set({'hour': 23, 'minute': 0, 'second': 0}).toDate();
        var data = { startDate, dueDate } //  id: _this.draggedItem, 
        _this.draggedItem = null;

        destinationColumn.classList.remove('hover');
        viewport.items.byColumnSelection = false;
        Timeline.Context.events.onItemDropped(data);

        return true;
    }

    _this.clear = function () {

        // Clear DOM structure
        _this.container.innerHTML = "";
    }

    _this.loadIndex = function (previous) {
        const { data } = Timeline.Context;

        if (previous && _this.startIndex == 0) return;
        if (!previous && _this.startIndex + _this.limit >= data.length - 1) return;

        var firstIndex = _this.findNext(_this.firstIndex.dataIndex, previous);
        var lastIndex = _this.findNext(_this.lastIndex.dataIndex, previous);

        if (firstIndex && lastIndex) {
            _this.firstIndex = firstIndex;
            _this.lastIndex = lastIndex;
        } else return null;

        return (previous ? firstIndex : lastIndex);
    }

    _this.findNext = function (lastIndex, previous) {
        const { data, indexes } = Timeline.Context;
        var hasItem = false;

        while(!hasItem) {
            if (previous && lastIndex == 0) break;
            if (!previous && lastIndex >= data.length - 1) break;

            var dataIndex = lastIndex + (previous ? -1 : 1);
            var item = data[dataIndex];

            if (!item) continue;

            if (item &&
                ((isNull(item.parent) || _this.expanded[item.parent] || !indexes[item.parent]) &&
                ((Timeline.Context.disableGroups || isNull(item.group)) || _this.expanded[`group${item.group}`]))) {
                hasItem = true;

                return { 
                    id: data[dataIndex].id,
                    index: previous ?
                        _this.startIndex - 1 :
                        _this.startIndex + _this.limit,
                    dataIndex: dataIndex
                };
            }

            lastIndex = dataIndex;
        }

        return null;
    }
    
    _this.render = function () {
        const { data, indexes } = Timeline.Context;

        // Calculate current view items limit
        _this.limit = Math.round(
            viewport.size.height / Timeline.Config.cellHeight) - 1;

        if (!data || !data.length) return;

        var limit = data.length < _this.limit ? data.length : _this.limit;

        if (_this.startIndex >= data.length || (_this.lastIndex && _this.lastIndex.index >= data.length)) {
            _this.startIndex = 0;
            _this.firstIndex = null;
            _this.lastIndex = null;
            //_this.expanded = {};
            Timeline.Context.itemsCount = 0;
        }

        var index = _this.startIndex;
        var dataIndex = _this.firstIndex ? _this.firstIndex.dataIndex : 0;

        while(limit) {
            var item = data[dataIndex];

            if (!item) break;

            if ((isNull(item.parent) || _this.expanded[item.parent] || !indexes[item.parent]) && ((Timeline.Context.disableGroups || isNull(item.group)) || _this.expanded[`group${item.group}`])) {
                var newIndex = { id: item.id, index: index, dataIndex: dataIndex };

                if (!_this.firstIndex) _this.firstIndex = newIndex;
                //if (limit == 1)
                _this.lastIndex = newIndex;

                _this.add(item, newIndex, 0);
                limit--;
                index++;
            } else if (_this.elements[item.id]) { //if (!isNull(item.parent) && _this.elements[item.id]) {
                _this.elements[item.id].data = item;

                if (!_this.elements[item.id].isRemoved)
                    _this.elements[item.id].isRemoved = true;

                    if (_this.elements[item.id].isExpanded)
                        _this.elements[item.id].expand();
            }

            dataIndex++;
        }

        // Recalculate other elements
        viewport.update();

        // Recalculate masks
        _this.ensureMasksPosition();
    }

    _this.add = function (data, index, direction, sibling) {

        // Item exists flag
        var elementExists = false;

        // Create new item or update data
        if (!_this.elements[data.id])
            if (data.isGroup)
                _this.elements[data.id] = new Timeline.Group(data);
            else if (data.isDummy)
                _this.elements[data.id] = new Timeline.Spacer(data);
            else
                _this.elements[data.id] = new Timeline.Item(data);
        else {
            _this.elements[data.id].data = data;

            elementExists = true;
        }
        
        // Get item object
        var item = _this.elements[data.id];

        // Initialize item
        item.init(index, elementExists, direction, sibling);

        // Count this item
        Timeline.Context.itemsCount++;

        // Return newly created item
        return item;
    }

    _this.changeScale = function () {
        Object.values(_this.elements).forEach(function (item) {

            // Recreate item with new data
            item.update(true);
        });

        // Force recalculation of control styles
        viewport.update();
    } 
    
    _this.scroll = function (direction) {
        const { data } = Timeline.Context;

        // Do not allow to scroll above first item
        if (direction && _this.startIndex == 0) return;

        // Find new item index
        var newIndex;

        if (direction) {

            // Get previous item index
            newIndex = _this.loadIndex(true);

            if (!newIndex) return;

            // Adjust start index and remove unvisible item
            _this.startIndex--;
            _this.elements[
                _this.container.children[
                    _this.container.children.length - 1
                ].id
            ].remove();

        } else {

            // Get next item index
            newIndex = _this.loadIndex(false);

            if (!newIndex) return;

            // Adjust start index and remove unvisible item
            _this.elements[_this.container.children[0].id].remove();
            _this.startIndex++;
        }

        // Append new item
        _this.add(data[newIndex.dataIndex], newIndex, direction);

        // Update root css variable
        document.documentElement.style.setProperty('--index', _this.startIndex);

        // TODO: update only connection / Update only new connection
        viewport.update();
    }

    _this.updateMaskTree = function (id) {
        const { indexes } = Timeline.Context;

        var mask = _this.masks[id];
        var currentItem = viewport.items.elements[id];

        if (!currentItem) return;

        var maskHeight = 0;

        if (isNull(currentItem.nextSiblingId)) {
            var nextIndex = _this.elements[_this.lastIndex.id].index.index;
            maskHeight = ((nextIndex - currentItem.index.index) + 1) * Timeline.Config.cellHeight;
        } else {
            var nextIndex = _this.elements[currentItem.nextSiblingId].index.index;
            maskHeight = (nextIndex - currentItem.index.index) * Timeline.Config.cellHeight;
        }

        mask.element.style.height = `${maskHeight}px`;

        if (!isNull(mask.parent) && indexes[mask.parent]) _this.updateMaskTree(mask.parent);
    }

    _this.ensureMasksPosition = function () {
        Object.keys(_this.masks).forEach((m, i) => {
            var maskElement = _this.elements[m];

            if (!maskElement || !_this.masks[m]) return;

            if (_this.masks[m].index.index !== maskElement.index.index) {
                _this.masks[m].index = maskElement.index;
                maskElement.ensureMask(true);
            }

            if (!Timeline.Context.indexes[m] && maskElement.isExpanded)
                maskElement.expand();
        });
    }

    _this.bringIntoView = function (args) {
        _this.getCurrentItem(args.composedPath());
        
        if (!_this.current) return;

        // Calculate new viewport position
        var space = Math.round(viewport.size.width / Timeline.Config.cellWidth);

        Timeline.Context.currentDate = _this.current.data.startDate ?
            _this.current.data.startDate : _this.current.data.dueDate;

        Timeline.Context.currentDate = 
            Timeline.Context.currentDate.addPeriod((space / 2));

        // Rerender viewport
        viewport.columns.clear();
        viewport.columns.render(Timeline.Context.currentDate);
        viewport.update();
    }

    _this.handleItemMovement = function (args) {

        // If no change then stop
        if (args.change.x == 0) return;

        // Get current interaction item
        if (!_this.current) _this.getCurrentItem(args.path);

        // Move item
        if (_this.current && !_this.current.isGroup)
            _this.current.move(args);
    }

    _this.handleItemResizing = function (args) {

        // If no change then stop
        if (args.change.x == 0) return;

        // Get current interaction item
        if (!_this.current) _this.getCurrentItem(args.path);

        // Resize item
        if (_this.current) _this.current.resize(args);
    }

    _this.getCurrentItem = function (itemPath) {
        var itemObj = itemPath.find(function (n) { return n.tagName == 'ITEM' });

        return _this.setCurrentItem(_this.elements[itemObj.id]);
    }

    _this.setCurrentItem = function (item) {
        _this.current = item;

        return _this.current;
    }

    _this.ensureNextItemDates = function (startId, endId) {

        // Get connection start and end nodes
        var startItem = _this.elements[startId];
        var endItem = _this.elements[endId];

        // If one of them not exists then exit
        if (!startItem || !endItem) return;
        
        // Get correct dates for start and end item
        var [startItemStartDate, startItemDueDate] = startItem.validateDates();
        var [endItemStartDate, endItemDueDate] = endItem.validateDates();

        var itemsDifference = startItemDueDate.diff(endItemStartDate, 'days') + 1;
        var endItemDiff = startItemDueDate.diff(endItemStartDate, 'days') + 1;

        if (itemsDifference >= 0) {
            endItem.data.startDate = startItem.data.dueDate.add(1, 'days').set({'hour': 9, 'minute': 0, 'second': 0});
            endItem.data.dueDate.add(endItemDiff, 'days').set({'hour': 23, 'minute': 0, 'second': 0});
        } else return;

        if (Timeline.Context.skipWeekends) {
            if (endItem.data.startDate && (endItem.data.startDate.format('dddd') == 'Saturday' || endItem.data.startDate.format('dddd') == 'Sunday'))
                if (endItem.data.startDate.format('dddd') == 'Saturday')
                    endItem.data.startDate = endItem.data.startDate.add(2, 'days');
                else if (endItem.data.startDate.format('dddd') == 'Sunday')
                    endItem.data.startDate = endItem.data.startDate.add(1, 'days');

            if (endItem.data.dueDate && (endItem.data.dueDate.format('dddd') == 'Saturday' || endItem.data.dueDate.format('dddd') == 'Sunday'))
                if (Timeline.Context.scale == TIMELINESCALE.DAY)
                    if (endItem.data.dueDate.format('dddd') == 'Saturday')
                        endItem.data.dueDate = endItem.data.dueDate.add(2, 'days');
                    else if (endItem.data.dueDate.format('dddd') == 'Sunday')
                        endItem.data.dueDate = endItem.data.dueDate.add(1, 'days');
                else
                    if (endItem.data.dueDate.format('dddd') == 'Saturday')
                        endItem.data.dueDate = endItem.data.dueDate.add(-1, 'days');
                    else if (endItem.data.dueDate.format('dddd') == 'Sunday')
                        endItem.data.dueDate = endItem.data.dueDate.add(-2, 'days');
        }

        Timeline.Context.events.onItemMoved({
            id: endId,
            startDate: endItem.data.startDate.toDate(),
            dueDate: endItem.data.dueDate.toDate()
        });
    }
}
