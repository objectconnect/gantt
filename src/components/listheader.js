import { UserActionEnum } from '../constants/enums';
import { generateFromTemplate, createInternalStyling } from '../helpers/helpers';

globalThis.Timeline.ListHeader = function () {

    var _this = this;

    _this.container;
    _this.gridHeader;

    _this.userAction = UserActionEnum.NONE;         // Current user interaction
    _this.movementInstance;                         // This movement helper instance

    // TODO: refactor
    _this.leftPanelWidth = 332;
    _this.sizes = {};

    _this.currentColumn = null;

    _this.init = function () {
        if (!Timeline.Config.showLeftPanel) {
            Timeline.Context.rootContainer.classList.add('fullView');
            return;
        }

        _this.createInternalStyling();
        _this.create();

        _this.movementInstance = new Timeline.MovementHelper(
            _this.container,
            null,
            _this.onMovementStarted,
            _this.onMovement,
            _this.onMovementFinished,
            false,
            null
        );
        _this.movementInstance.init();
    }

    _this.createInternalStyling = function () {
        Timeline.Context.viewport.internalStyleSheets['ITEM_DATAGRID'] =
            createInternalStyling('ITEM_DATAGRID');
    }

    _this.updateStyling = function (args) {

        if (!_this.sizes[0]) _this.sizes[0] = 180;

        var defaultColumnSize = 100;
        var columnsStylingString = `--column0: ${args && args[0] ? args[0] : _this.sizes[0]}px;`;

        // Create styling based on input data
        Timeline.Context.viewColumns.forEach((e, i) => {
            var x = i + 1;
            var size = args && args[x] ? args[x] : _this.sizes[x] ? _this.sizes[x] : defaultColumnSize;

            if (!_this.sizes[x]) _this.sizes[x] = size;
            columnsStylingString += `--column${x}: ${size}px;`;
        });

        // Create styles as global
        var styles = `:root {` + 
                        `--leftPanelWidth: ${args && args.leftPanelWidth ? args.leftPanelWidth : _this.leftPanelWidth}px;` +
                        columnsStylingString +
                     '}';

        // Update document css rules
        Timeline.Context.viewport.internalStyleSheets['ITEM_DATAGRID'].deleteRule(0);
        Timeline.Context.viewport.internalStyleSheets['ITEM_DATAGRID'].insertRule(styles);
    }

    _this.create = function () {
        _this.container = generateFromTemplate(Timeline.Templates.listTemplate)[0];
        Timeline.Context.rootContainer.appendChild(_this.container);

        _this.createGridHeader();

        _this.container.children[2].onclick = function () {
            if (Timeline.Context.rootContainer.classList.contains('hideItemHeader'))
                Timeline.Context.rootContainer.classList.remove('hideItemHeader');
            else Timeline.Context.rootContainer.classList.add('hideItemHeader');

            Timeline.Context.refresh();
        }
    }

    _this.createGridHeader = function () {
        
        _this.updateStyling();

        _this.gridHeader = _this.container.children[1];

        var titleColumn = generateFromTemplate(
            Timeline.Templates.gridColumnTemplate.format('Title', 0))[0];

        _this.gridHeader.appendChild(titleColumn);

        var size = _this.sizes[0] + 10;
        Timeline.Context.viewColumns.forEach((e, i) => {
            var column = generateFromTemplate(
                Timeline.Templates.gridColumnTemplate.format(e.displayName, i + 1))[0];

            size += _this.sizes[i + 1];
            size += 10;
            _this.gridHeader.appendChild(column);
        });

        _this.leftPanelWidth = size + 100;
        _this.updateStyling();
    }

    _this.onMovementStarted = function (args) {
        
        // Based on event target select interaction type
        _this.detectUserInteraction(args);
    }

    _this.onMovement = function (args) {

        switch (_this.userAction) {
            case UserActionEnum.RESIZE_LEFTPANEL:
                // Current movement change
                var change = args.mouse.x - args.startPosition.x;

                var newValue = _this.leftPanelWidth + change < 100 ? 
                    100 : _this.leftPanelWidth + change;

                _this.updateStyling({ leftPanelWidth: newValue });
                break;
            case UserActionEnum.RESIZE_COLUMN:
            
                // Current movement change
                var change = args.mouse.x - args.startPosition.x;

                var newValue = _this.sizes[_this.currentColumn] + change < 5 ? 
                    5 : _this.sizes[_this.currentColumn] + change;

                _this.updateStyling({ [_this.currentColumn]: newValue });
                break;
        }
    }

    _this.onMovementFinished = function (args) {

        // Current movement change
        switch (_this.userAction) {
            case UserActionEnum.RESIZE_LEFTPANEL:
                var change = args.default.clientX - args.startPosition.x;

                _this.leftPanelWidth += change;
                
                Timeline.Context.refresh();
                break;
            
            case UserActionEnum.RESIZE_COLUMN:
                var change = args.default.clientX - args.startPosition.x;

                var newValue = _this.sizes[_this.currentColumn] + change < 5 ? 
                    5 : _this.sizes[_this.currentColumn] + change;

                _this.sizes[_this.currentColumn] = newValue;
                break;
        }

        // Reset user action info
        _this.userAction = UserActionEnum.NONE;
        _this.currentColumn = null;
    }

    _this.detectUserInteraction = function (args) {
        
        // Draw new connection from selected item
        if (args.target.tagName == "RESIZE" && args.target.className == 'leftpanel')
            _this.userAction = UserActionEnum.RESIZE_LEFTPANEL;

        if (args.target.tagName == "RESIZE" && args.target.className == 'column') {
            _this.userAction = UserActionEnum.RESIZE_COLUMN;
            _this.currentColumn = args.target.attributes['data-count'].value;
        }
    }
}