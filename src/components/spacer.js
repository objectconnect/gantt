import { generateFromTemplate } from '../helpers/helpers';

globalThis.Timeline.Spacer = function (data) {

    var _this = this;

    var { cellHeight } = Timeline.Config;
    var { viewport } = Timeline.Context;

    _this.data = data;                  // Data object
    _this.container;                    // Item DOM container
    _this.index;                        // Current item index

    _this.isRemoved = false;            // If item is removed from DOM

    _this.init = function (index, recreate, direction, sibling) {
        if (!_this.container || recreate) _this.create();

        _this.isRemoved = false;
        _this.index = index;

        _this.append(direction, sibling);
    }
    
    _this.create = function () {
        if (!_this.data) return;

        // Generate item DOM
        _this.container = generateFromTemplate(
            Timeline.Templates.dummyItemTemplate.format(_this.data.id))[0];
    }

    _this.remove = function () {

        // Set removed flag
        _this.isRemoved = true;

        // Remove from parent container
        viewport.items.container.removeChild(_this.container);

        // Adjust items counter
        Timeline.Context.itemsCount--;
    }

    // TODO: Refactor with writing XML structure
    _this.append = function (direction, sibling) {

        // If item should be inserted before any item
        if (sibling)
            // Add based on current direction
            if (!direction)
                viewport.items.container.insertBefore(
                    _this.container, sibling.nextSibling);
            else
                viewport.items.container.insertBefore(
                    _this.container, sibling);
        else
            // Add based on current direction
            if (!direction)
                viewport.items.container.appendChild(
                    _this.container);
            else
                viewport.items.container.insertBefore(
                    _this.container, viewport.items.container.children[0]);
    }

    _this.update = function (
        recreate,                   // If item need to be recreated
        args                        // Interaction args
    ) {

        if (recreate) {
            var old = _this.container;

            // Recreate item
            _this.create();

            if (!_this.removed)
                viewport.items.container.replaceChild(_this.container, old);

            viewport.update();
            
            return;
        }
    }
}