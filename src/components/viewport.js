import { UserActionEnum } from '../constants/enums';
import {
    getIdFromDate,
    getDatesRange,
    generateFromTemplate,
    createInternalStyling
} from '../helpers/helpers';

globalThis.Timeline.Viewport = function (context) {

    var _this = this;

    _this.container;                                // Viewport root DOM element
    _this.size;                                     // Information about viewport position and size

    _this.viewStartDate;                            // Timeline start date
    _this.viewEndDate;                              // Timeline end date

    _this.userAction = UserActionEnum.NONE;         // Current user interaction
    _this.startElement;                             // Holds element name where interaction started
    _this.current;                                  // Current interaction item
    _this.movementInstance;                         // This movement helper instance
    _this.internalStyleSheets = {};                 // Internal styling

    // Viewport components
    _this.columns;
    _this.items;
    _this.connections;

    Timeline.Context.dates = {};                    // Dates
    Timeline.Context.coordinates = {};              // Columns coordinates

    _this.init = function () {

        // Add base viewport structure
        _this.create();

        // Initialize
        _this.columns = new Timeline.Columns();
        _this.items = new Timeline.Items();
        _this.connections = new Timeline.Connections();

        _this.items.init();
        _this.connections.init();

        // Attach to mouse wheel event
        Timeline.onMouseWheel = _this.handleMouseWheel;

        // Initialize Movement helper
        _this.movementInstance = new Timeline.MovementHelper(
            _this.container,
            null,
            _this.onMovementStarted,
            _this.onMovement,
            _this.onMovementFinished,
            false,
            null
        );
        _this.movementInstance.init();
    }

    _this.create = function () {
        _this.container = generateFromTemplate(Timeline.Templates.viewportTemplate)[0];
        context.rootContainer.appendChild(_this.container);

        _this.internalStyleSheets['VIEWPORT'] = createInternalStyling('VIEWPORT');
        _this.readViewportSize();
    }

    _this.readViewportSize = function () {
        var containerBoundingBox = context.rootContainer.getBoundingClientRect();

        _this.size = {
            width: _this.container.clientWidth,
            height: _this.container.clientHeight,
            offsetTop: _this.container.offsetTop,
            offsetLeft: _this.container.offsetLeft + containerBoundingBox.left,
            parentTop: containerBoundingBox.top - 10,
            parentLeft: containerBoundingBox.left
        };

        document.documentElement.style.setProperty('--controlLeft', _this.size.parentLeft + 'px');
        document.documentElement.style.setProperty('--cellHeight', Timeline.Config.cellHeight + 'px');
    }

    _this.onMovementStarted = function (args) {

        // Based on event target select interaction type
        _this.detectUserInteraction(args);
    }

    _this.onMovement = function (args) {
        
        // Ony by column selection
        if (_this.items.byColumnSelection) {

            // Columns selection ( only by specific place/interaction )
            _this.handleColumnSelection(args);

            // Stop movement heare
            return;
        }

        switch(_this.userAction) {
            case UserActionEnum.MOVE_VIEWPORT:
                _this.handleViewportMovement(args);
                break;
            case UserActionEnum.MOVE_NODE:
                _this.items.handleItemMovement(args);
                break;
            case UserActionEnum.RESIZE_NODE:
                _this.items.handleItemResizing(args);
                break;
            case UserActionEnum.DRAW_CONNECTION:
                _this.connections.new(args);
                break;

            default:
                break;
        }
    }

    _this.onMovementFinished = function (args) {
        if (_this.items.byColumnSelection)
            _this.items.current.finishDateSelection(_this.startElement);

        if (_this.userAction == UserActionEnum.ACTIVATE_ITEM)
            _this.items.bringIntoView(args.default);

        if (_this.userAction == UserActionEnum.DRAW_CONNECTION)
            _this.connections.finishNewConnection();

        // Finish item actions
        if (_this.items.current &&
            !_this.items.current.isGroup &&
            _this.userAction != UserActionEnum.NONE &&
            _this.userAction !== UserActionEnum.ACTIVATE_ITEM)
            _this.items.current.finishAction(_this.userAction);

        // Reset current interaction item
        _this.items.setCurrentItem(null);

        // Reset interaction start element
        _this.startElement = null;

        // Reset user action info
        _this.userAction = UserActionEnum.NONE;
    }

    _this.configureEvents = function (passTrought) {

        // Unblock movement events
        if (passTrought) {
            Timeline.currentTarget = Object.keys(Timeline.movementInstances)[1];
            _this.movementInstance.passTrought = true;
        } else {
            // TODO: If we need this part ?
            Timeline.currentTarget = null;
            _this.movementInstance.passTrought = false;
        }
    }

    _this.handleColumnSelection = function (args) {

        // Find index of current column by calculating event data
        var column = Math.floor((args.mouse.x - _this.size.offsetLeft) / Timeline.Config.cellWidth);

        // If there is hovered column
        if (_this.startElement && _this.startElement.classList)
            _this.startElement.classList.remove('hover');

        // Set current column as hovered
        if (column >= 0) {
            _this.startElement = _this.columns.container.children[column];
            _this.startElement.classList.add('hover');
        }
    }

    _this.handleViewportMovement = function (args) { 
        for (var i = 0; i < args.change.x; i++)
            _this.columns.update(!args.direction.x);
        
        for (var i = 0; i < args.change.y; i++)
            _this.items.scroll(!args.direction.y);
    
        if (args.change.x != 0)
            _this.update();

        if (args.change.x == 0 && args.change.y != 0)
            _this.connections.update()
    }

    _this.handleMouseWheel = function (args) {
        if (args.deltaY > 0)
            _this.items.scroll(0);

        if (args.deltaY < 0)
            _this.items.scroll(1);

        _this.connections.update()
    }

    _this.detectUserInteraction = function (args) {
        
        // Draw new connection from selected item
        if (args.target.tagName == "ATTACHLEFT" ||
            args.target.tagName == "ATTACHRIGHT")
            _this.userAction = UserActionEnum.DRAW_CONNECTION;

        // Expand item
        else if (args.target.tagName == "EXPAND") _this.userAction = UserActionEnum.NONE;

        // Bring intem back into view
        else if (args.target.tagName == "INDICATOR")
            _this.userAction = UserActionEnum.ACTIVATE_ITEM;

        // Resize item
        else if (args.target.tagName == "RESIZELEFT" ||
                 args.target.tagName == "RESIZERIGHT")
            _this.userAction = UserActionEnum.RESIZE_NODE;

        // Move item
        else if (args.target.parentElement.parentElement.tagName == "SHAPE" ||
                 args.target.parentElement.tagName == "SHAPE" ||
                 args.target.tagName == "SHAPE")
            _this.userAction = UserActionEnum.MOVE_NODE;

        // Move viewport ( items, columns, etc. )
        else _this.userAction = UserActionEnum.MOVE_VIEWPORT;

        // Store interaction start element
        _this.startElement = _this.items.byColumnSelection ?
            _this.startElement : args.target.tagName;

        //if (!_this.userAction)
            // Timeline.Context.dynamicInteraction = true;
    }

    _this.update = function () {
        var styles = _this.calculate();

        // Recalculate connections
        styles += _this.connections.update(true);

        // Pack styles as global
        styles = `:root { ${styles} }`;

        // Update document css rules
        _this.internalStyleSheets['VIEWPORT'].deleteRule(0);
        _this.internalStyleSheets['VIEWPORT'].insertRule(styles);
    }

    _this.calculate = function () {

        // Make a copy of current required dates
        var datesCopy = Object.assign({}, Timeline.Context.dates);
                
        // Get current visible dates range
        var startDate = _this.viewStartDate;
        var endDate = _this.viewEndDate;

        // Get viewport range
        var range = getDatesRange(startDate, endDate);

        // Generated current frame styles
        var styles = '';

        // Generete styles for current frame
        for (var x = 0; x < range + 1; x++) {
            var currentDate = x === 0 ? startDate : startDate.addPeriod(x);

            var styleId = getIdFromDate(currentDate);
            styles += ` --${styleId}: ${x * 50}px; `;

            // Store style internaly
            Timeline.Context.coordinates[styleId] = x * 50;

            if(datesCopy[styleId]) delete datesCopy[styleId];
        }

        // Ensure required styles for not visible columns
        Object.keys(datesCopy).forEach(function (d, i) {
            var offset = 0;

            // If date smaller then viewport start date
            if (datesCopy[d].valueOf() < startDate.valueOf())
                offset = -19;

            // If date bigger then viewport end date
            if (datesCopy[d].valueOf() >= endDate.valueOf())
                offset = _this.size.width + 20;//12;

            // Append new style
            styles += ` --${d}: ${offset}px; `;

            // Store style internaly
            Timeline.Context.coordinates[d] = offset;
        });

        return styles;
    }
}
