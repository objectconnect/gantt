globalThis.Timeline.Configuration = function() {
    return {
        "logDetails": false,
        "showLeftPanel": true,
        "allowToHideLeftPanel": false,
        "cellWidth": 50,
        "cellHeight": 35,
        "shapeMargin": 5,
        "days": [
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat'
        ],
        "months": [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ],
        "shortMonths": [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ]
    };
}