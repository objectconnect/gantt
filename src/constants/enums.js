export const UserActionEnum = {
    NONE: 0,
    MOVE_VIEWPORT: 1,
    MOVE_NODE: 2,
    RESIZE_NODE: 3,
    DRAW_CONNECTION: 4,
    SHOW_HIDE_CHILDREN: 5,
    ACTIVATE_ITEM: 6,
    RESIZE_LEFTPANEL: 7,
    RESIZE_COLUMN: 8
}

export const DIRECTION = {
    LEFT: 0,
    RIGHT: 1,
    TOP: 2,
    BOTTOM: 3
}

export const TIMELINESCALE = {
    DAY: 0,
    WEEK: 1,
    //TWO_WEEKS: 2,
    MONTH: 2,
    QUARTER: 3,
    HALF_YEAR: 4,
    YEAR: 5
}

export const ROWHEIGHT = {
    s: 30,
    m: 35,
    l: 40,
    xl: 45
}