export function initializeEvents() {
    Timeline.Context.events = {
        onNewItem: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onNewItem", { detail, bubbles: true, cancelable: true })); },
        onItemRender: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemRender", { detail, bubbles: true, cancelable: true })); },
        onValueRender: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onValueRender", { detail, bubbles: true, cancelable: true })); },
        onItemDestroy: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemDestroy", { detail, bubbles: true, cancelable: true })); },
        onClean: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onClean", { detail, bubbles: true, cancelable: true })); },
        onItemStartDateChanged: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemStartDateChanged", { detail, bubbles: true, cancelable: true })); },
        onItemDueDateChanged: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemDueDateChanged", { detail, bubbles: true, cancelable: true })); },
        onItemMoved: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemMoved", { detail, bubbles: true, cancelable: true })); },
        onItemClick: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemClick", { detail, bubbles: true, cancelable: true })); },
        onGroupExpanded: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onGroupExpanded", { detail, bubbles: true, cancelable: true })); },
        onViewportMove: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onViewportMove", { detail, bubbles: true, cancelable: true })); },
        onViewportMoved: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onViewportMoved", { detail, bubbles: true, cancelable: true })); },
        onNodeMove: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onNodeMove", { detail, bubbles: true, cancelable: true })); },
        onNodeMoved: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onNodeMoved", { detail, bubbles: true, cancelable: true })); },
        onZoomChange: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onZoomChange", { detail, bubbles: true, cancelable: true })); },
        onNewRelation: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onNewRelation", { detail, bubbles: true, cancelable: true })); },
        onRelationDeleted: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onRelationDeleted", { detail, bubbles: true, cancelable: true })); },
        onDrop: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onDrop", { detail, bubbles: true, cancelable: true })); },
        onLoad: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onLoad", { detail, bubbles: true, cancelable: true })); },
        onViewChanged: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onViewChanged", { detail, bubbles: true, cancelable: true })); },
        onItemDropped: function (detail) { Timeline.Context.rootContainer.dispatchEvent(new CustomEvent("Timeline.onItemDropped", { detail, bubbles: true, cancelable: true })); },
    };
}