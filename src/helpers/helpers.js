import { TIMELINESCALE } from '../constants/enums';
import moment from 'moment';

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) { 
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    };
}

moment.prototype.addPeriod = function (amount) {
    let date = this.clone();

    switch(Timeline.Context.scale) {
        case TIMELINESCALE.DAY:
            date.add(amount, 'days');
            break;
        case TIMELINESCALE.WEEK:
            date.add(amount, 'weeks');
            break;
        case TIMELINESCALE.TWO_WEEKS:
            date.add(amount * 2, 'weeks');
            break;
        case TIMELINESCALE.MONTH:
            date.add(amount, 'months');
            break;
        case TIMELINESCALE.QUARTER:
            date.add(amount, 'quarters');
            break;
        case TIMELINESCALE.HALF_YEAR:
            date.set('date', 1);
            if (getHalfYear(date)) date.set('month', 6);
            else date.set('month', 0);

            date.add(amount * 6, 'months');
            break;
        case TIMELINESCALE.YEAR:
            date.add(amount, 'years');
            break;
    }

    return date;
}

export function getColumnData (date) {
    var columnId = getIdFromDate(date); 
        
    // Save info about requested date
    Timeline.Context.dates[columnId] = date;

    return {
        id: columnId,
        object: Timeline.Context.viewport.columns.elements[columnId]
    };
 }

export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

export function getDatesRange(startDate, endDate) {
    if (!startDate || !endDate) return;

    switch(Timeline.Context.scale) {
        case TIMELINESCALE.DAY:
            return endDate.diff(startDate, 'days');
        case TIMELINESCALE.WEEK:
            return endDate.diff(startDate, 'week');
        case TIMELINESCALE.TWO_WEEKS:
            return endDate.diff(startDate, 'week') / 2;  
        case TIMELINESCALE.MONTH:
            return endDate.diff(startDate, 'month');
        case TIMELINESCALE.QUARTER:
            return endDate.diff(startDate, 'quarter');
        case TIMELINESCALE.HALF_YEAR:
            return Math.round(endDate.diff(startDate, 'months') / 6);
        case TIMELINESCALE.YEAR:
            return endDate.diff(startDate, 'year');
    }
}

export function getHalfYear(date) {
    var q = [0, 1];
    return q[Math.floor(date.month() / 6)];
}

export function getStartPositionWithDirection(args) {
    var current = Timeline.Context.viewport.items.current;

    if (!current) {
        current = Timeline.Context.viewport.items.getCurrentItem(args.path);

        var direction = 0;
        var startPosition = { x: 0, y: 0 };

        var { tagName } = args.default.target;
        var { startIndex } = Timeline.Context.viewport.items;
        var { cellHeight } = Timeline.Config;

        direction = tagName == 'ATTACHRIGHT';

        if (direction)
            startPosition = {
                x: Timeline.Context.coordinates[current.endColumn.id] + 10,
                y: ((current.index.index - startIndex) * cellHeight)
            };

        else
            startPosition = {
                x: Timeline.Context.coordinates[current.startColumn.id],
                y: ((current.index.index - startIndex) * cellHeight)
            };
        
        // TODO: remove
        //startPosition.y += 15;//Timeline.Context.viewport.size.parentTop;
        startPosition.y += Timeline.Config.cellHeight / 2
        
        current.container.classList.add('newConnection');

        return [direction, startPosition];
    }
}

export function createConnectionStructure(startPosition) {
    var namespace = 'http://www.w3.org/2000/svg';

    var result = document.createElementNS(namespace, 'svg');
    result.id = 'newConnection';

    var path = document.createElementNS(namespace, 'path');
    path.style.cssText =
        'stroke: #000; ' +
        'stroke-width: 1px; ' +
        'stroke-dasharray: 10,10; ' +
        'fill: none; ';
    result.appendChild(path);

    var circle = document.createElementNS(namespace, 'circle');
    circle.style.cssText =
        'fill: black; ' +
        `cx: ${startPosition.x}; ` +
        `cy: ${startPosition.y}; ` +
        'r: 8; ';
    result.appendChild(circle);

    Timeline.Context.viewport.container.appendChild(result);

    return result;
}

export function generateFromTemplate (template) {
    var wrapper = document.createElement('TemplateWrapper');
    wrapper.innerHTML = template;
    return wrapper.childNodes;
}

export function getDateFromId (stringDate) {
    var dateParts = stringDate.split('_');
    
    switch(Timeline.Context.scale) {
        case TIMELINESCALE.DAY:
            return moment({ year: dateParts[2], month: dateParts[1], day: dateParts[0] }).locale('en-gantt');
        case TIMELINESCALE.WEEK:
            return moment().locale('en-gantt').isoWeekYear(dateParts[1]).isoWeek(dateParts[0]).isoWeekday(1).clone().startOf('day');
        case TIMELINESCALE.TWO_WEEKS:
            return moment().locale('en-gantt').isoWeekYear(dateParts[1]).isoWeek(dateParts[0]).isoWeekday(0).clone().startOf('day');
        case TIMELINESCALE.MONTH:
            return moment({ year: dateParts[1], month: dateParts[0], day: 1 }).locale('en-gantt');
        case TIMELINESCALE.QUARTER:
            var quarter = dateParts[0].split('Q')[1];
            return moment({ year: dateParts[1] }).locale('en-gantt').quarter(parseInt(quarter));
        case TIMELINESCALE.HALF_YEAR:
            return moment({ year: dateParts[1], month: parseInt(dateParts[0]) ? 6 : 0 }).locale('en-gantt');
        case TIMELINESCALE.YEAR:
            return moment({ year: dateParts[0], month: 1, day: 1 }).locale('en-gantt');
    }
}

export function ensureDateByScale (date, direction) {
    switch(Timeline.Context.scale) {
        case TIMELINESCALE.DAY:
            return date;
        case TIMELINESCALE.WEEK:
            return date[direction]('isoWeek');
        case TIMELINESCALE.TWO_WEEKS:
            return date[direction]('isoWeek');
        case TIMELINESCALE.MONTH:
            return date[direction]('month');
        case TIMELINESCALE.QUARTER:
            return date[direction]('quarter');
        case TIMELINESCALE.HALF_YEAR:
            if (direction == 'endOf')
                return date.clone().add(5, 'months').endOf('month');
            else
                return date.startOf('month');
        case TIMELINESCALE.YEAR:
            return date[direction]('year');
    }
}

export function getIdFromDate (date) {
    var { scale } = Timeline.Context;

    if (!scale) 
        return "{0}_{1}_{2}".format(
            date.get('date'),
            date.get('month'),
            date.get('year')
        );
    else             
        switch(scale) {
            case TIMELINESCALE.DAY:
                return "{0}_{1}_{2}".format(
                    date.get('date'),
                    date.get('month'),
                    date.get('year')
                );
            case TIMELINESCALE.WEEK:
                return "{0}_{1}".format(
                    date.isoWeek(),
                    date.isoWeekYear() // iso start always at monday
                );
            case TIMELINESCALE.TWO_WEEKS:
                return "{0}_{1}".format(
                    date.isoWeek(),
                    date.isoWeekYear()
                );
            case TIMELINESCALE.MONTH:
                return "{0}_{1}".format(
                    date.get('month'),
                    date.get('year')
                );
            case TIMELINESCALE.QUARTER:
                return "Q{0}_{1}".format(
                    date.get('quarter'),
                    date.get('year')
                );
            case TIMELINESCALE.HALF_YEAR:
                return "{0}_{1}".format(
                    date.quarter() <= 2 ? "0" : "1",
                    date.year()
                );
            case TIMELINESCALE.YEAR:
                return "{0}".format(date.get('year'));
        }
}

export function calculateNewDate(scale, date, change, direction) {
    change = direction ? -change : change;

    switch(scale) {
        case TIMELINESCALE.DAY:
            return date.clone().add(change, 'days');
        case TIMELINESCALE.WEEK:
            return date.clone().add(change, 'weeks').startOf('isoWeek');
        case TIMELINESCALE.MONTH:
            return date.clone().add(change, 'months').startOf('month');
        case TIMELINESCALE.QUARTER:
            return date.clone().add(change, 'quarters').startOf('quarter');
        case TIMELINESCALE.HALF_YEAR:
            return date.clone().add(change * 6, 'months').startOf('month');
        case TIMELINESCALE.YEAR:
            return date.clone().add(change, 'years').startOf('year');
    }
}

export function createSVGSpace(container) {
    var svgNamespace = "http://www.w3.org/2000/svg";
        var newContainer = document.createElementNS(svgNamespace, 'svg');

        var marker = document.createElementNS(svgNamespace, 'marker');
        marker.setAttribute("id", "head");
        marker.setAttribute("orient", "auto");
        marker.setAttribute("markerWidth", "5");
        marker.setAttribute("markerHeight", "10");
        marker.setAttribute("stroke-width", '1');
        marker.setAttribute("markerUnits", "userSpaceOnUse");
        marker.setAttribute("refX", "0.1");
        marker.setAttribute("refY", "5");

        var markerPath = document.createElementNS(svgNamespace, "path");
        markerPath.setAttribute("d", "M0,0 V10 L5,5 Z");
        markerPath.setAttribute("fill", "black");
        //markerPath.className = 'markerPath';
        marker.appendChild(markerPath);
        newContainer.appendChild(marker);

        marker = document.createElementNS(svgNamespace, 'marker');
        marker.setAttribute("id", "headHovered");
        marker.setAttribute("orient", "auto");
        marker.setAttribute("markerWidth", "5");
        marker.setAttribute("markerHeight", "10");
        marker.setAttribute("stroke-width", '1');
        marker.setAttribute("markerUnits", "userSpaceOnUse");
        marker.setAttribute("refX", "0.1");
        marker.setAttribute("refY", "5");

        markerPath = document.createElementNS(svgNamespace, "path");
        markerPath.setAttribute("d", "M0,0 V10 L5,5 Z");
        markerPath.setAttribute("fill", "blue");
        //markerPath.className = 'markerPath';
        marker.appendChild(markerPath);

        newContainer.appendChild(marker);
        container.appendChild(newContainer);

        return newContainer;
}

export function describePath(startPoint, endPoint, cellWidth, cellHeight) {
    var sX = startPoint.x;
    var sY = startPoint.y;
    var eX = endPoint.x;
    var eY = endPoint.y;

    var pathData = `M${sX - cellWidth},${sY} L${sX + (cellWidth / 2)},${sY}`;

    // if end point is bigger then start point
    if (sY < eY) {
        pathData += ' a8,8 0 0 1 8,8';
    
        // If start point is bigger then end point
        if (sX + cellWidth >= eX) {
            pathData += ' a8,8 0 0 1 -8,8';
            pathData += ` L${(eX - cellWidth) + 8} ${sY + (8 * 2)}`;
            pathData += ' a8,8 1 0 0 -8,8';
            pathData += ` L${endPoint.x - cellWidth},${endPoint.y - 8}`;
            pathData += ' a8,8 1 0 0 8,8';
        } else {
            pathData += ` L${(sX + (cellWidth / 2)) + 8},${eY - 8} a8,8 1 0 0 8,8`;
        }
    } else {
        pathData += ' a8,8 1 0 0 8,-8';

        // If start point is bigger then end point
        if (sX + cellWidth >= eX) {
            pathData += ' a8,8 1 0 0 -8,-8';
            
            pathData += ` L${(eX - cellWidth) + 8} ${sY - (8 * 2)}`;
            pathData += ' a8,8 0 0 1 -8,-8';
            pathData += ` L${eX - cellWidth},${eY + 8}`;
            pathData += ' a8,8 0 0 1 8,-8';
        } else {
            pathData += ` L${(sX + (cellWidth / 2)) + 8},${eY + 8}`;
            pathData += ' a8,8 0 0 1 8,-8';
        }
    }

    // Draw last line to the end node
    pathData += ` L${eX},${eY}`;

    // Return the result path
    return pathData;
}

export function createCurveTwoPoints(element, p1x, p1y, p2x, p2y, directionX) {
    p1x = !directionX ? p1x - 10 : p1x;

    var newP1x = p1x;

    // mid-point of line:
    var mpx = (p2x + newP1x) * 0.5;
    var mpy = (p2y + p1y) * 0.5;

    // angle of perpendicular to line:
    var theta = Math.atan2(p2y - p1y, p2x - newP1x) - Math.PI / 2;

    // distance of control point from mid-point of line:
    var offset = (p2y - p1y) / 2;

    // location of control point:
    var c1x = mpx + offset * Math.cos(theta);
    var c1y = mpy + offset * Math.sin(theta);

    // construct the command to draw a quadratic curve
    var curve = "M" + p1x + " " + p1y + "L " + newP1x + " " + p1y + " Q " + c1x + " " + c1y + " " + p2x + " " + p2y;

    element.setAttribute("d", curve);

    return element;
}

export function getColumnContent(date) {
    let [timePart, classes, content] = Array(6).fill('');
    const { scale, today } = Timeline.Context;
    
    switch(scale) {
        case TIMELINESCALE.DAY:
            if (today.isSame(date, 'days'))
                classes += ' today ';

            if (date.get('date') == 1) {
                timePart = '<new>{0} {1}</new>'.format(
                    Timeline.Config.months[date.get('month')],
                    date.get('year')
                );
                classes += ' newTimePart ';
            }

            if (date.isoWeekday() == 6 || date.isoWeekday() == 7)
                classes += ' weekend ';
            
            content = '<span>{0}</span><span>{1}</span>'.format(
                date.get('date'),
                Timeline.Config.days[date.get('day')]
            );
            break;
        case TIMELINESCALE.WEEK:
            if (moment(today).locale('en-gantt').isoWeek() == date.isoWeek() &&
                moment(today).locale('en-gantt').isoWeekYear() == date.isoWeekYear())
                classes += ' today ';
            
            if (date.isSame(date.clone().endOf('week').clone().startOf('month'), 'week')) {
                var month = date.clone().endOf('week').clone().startOf('month');
                timePart = '<new>{0} {1}</new>'.format(
                    Timeline.Config.months[month.month()],
                    month.year()
                );
                classes += ' newTimePart ';
            }
            
            content = '<span>{0}</span>'.format(date.isoWeek());
            break;
        case TIMELINESCALE.TWO_WEEKS:
            var newDate = moment(date).locale('en-gantt');

            if ((moment(today).locale('en-gantt').week() == newDate.week() &&
                moment(today).locale('en-gantt').weekYear() == newDate.weekYear()) ||
                (moment(today).locale('en-gantt').week() == newDate.clone().add(1, 'week').week() &&
                moment(today).locale('en-gantt').weekYear() == newDate.clone().add(1, 'week').weekYear()))
                classes += ' today ';
            
            if (newDate.isSame(newDate.clone().endOf('week').clone().startOf('month'), 'week') ||
                newDate.clone().add(1, 'week').isSame(newDate.clone().add(1, 'week').endOf('week').startOf('month'), 'week')) {
                var month = newDate.isSame(newDate.clone().endOf('week').clone().startOf('month'), 'week') ?
                    newDate.clone().endOf('week').clone().startOf('month') :
                    newDate.clone().add(1, 'week').endOf('week').startOf('month');

                timePart = '<new>{0} {1}</new>'.format(
                    Timeline.Config.months[month.month()],
                    month.year()
                );
                classes += ' newTimePart ';
            }
            
            content = '<span>{0}</span>'.format(moment(date).locale('en-gantt').week());
            break;
        case TIMELINESCALE.MONTH:
            if (date.isSame(today, 'month'))
                classes += ' today ';

            if (date.get('months') == 0) {
                timePart = '<new>{0}</new>'.format(date.get('year'));
                classes += ' newTimePart ';
            }
            
            content = '<span>{0}</span>'.format(Timeline.Config.shortMonths[date.get('months')]);
            break;
        case TIMELINESCALE.QUARTER:
            if (date.isSame(today, 'quarter'))
                classes += ' today ';

            if (date.get('quarter') == 1) {
                timePart = '<new>{0}</new>'.format(date.get('year'));
                classes += ' newTimePart ';
            }
            
            content = '<span>Q{0}</span>'.format(date.get('quarter'));
            break;
        case TIMELINESCALE.HALF_YEAR:
            if (today.isSame(date, 'year') &&
                getHalfYear(today) == getHalfYear(date))
                classes += ' today ';

            if (getHalfYear(date) == 0) {
                timePart = '<new>{0}</new>'.format(date.year());
                classes += ' newTimePart ';
            }
            
            content = '<span>{0}</span>'.format(date.quarter() <= 2 ? 'H1' : 'H2');
            break;
        case TIMELINESCALE.YEAR:
            if (date.isSame(today, 'year'))
                classes += ' today ';

            content = '<span>{0}</span>'.format(date.get('year'));
            break;
    }

    return [timePart, classes, content];
}

export function isNull(value) {
    return value == null || value == undefined;
}

export function getLargestDates(old, a, b) {
    var startDate, dueDate;

    if (old) {
        if (!old.startDate) startDate = a;
        if (!a) startDate = old.startDate;

        if (!old.dueDate) dueDate = b;
        if (!b) dueDate = old.dueDate;

        if (old.startDate && a)
            if (old.startDate.diff(a, 'days') > 0) startDate = a;
            else startDate = old.startDate;

        if (old.dueDate && b)
            if (b.diff(old.dueDate, 'days') > 0) dueDate = b;
            else dueDate = old.dueDate;

    } else {
        startDate = a;
        dueDate = b;
    }

    return {
        startDate: startDate,
        dueDate: dueDate
    };
}

export function createIndexes(inputData) {
    var indexes = {};
    var groups = {};
    var emptyNodes = {}; // Not used as currently

    inputData.forEach((e, i) => {
        if (!Timeline.Context.showCardsWithoutDates && (!e.startDate && !e.dueDate)) {
            emptyNodes[e.id] = e;
            return;
        }

        if (e.startDate) e.startDate = moment(e.startDate).locale('en-gantt');
        if (e.dueDate) e.dueDate = moment(e.dueDate).locale('en-gantt');

        if (!isNull(e.group))
            groups[e.group] = getLargestDates(groups[e.group], e.startDate, e.dueDate);

        indexes[e.id] = { data: e, index: i };
    });

    return [indexes, emptyNodes, groups];
}

export function randomNumber(min, max) { 
    return Math.floor(Math.random() * (max - min) + min);
}

export function assignValue(val) {
    if(val === null) return Infinity;
    else return val;
}

export function sortByDates(collection) {
    return collection.sort((a, b) => {
        var aDate = assignValue(a.startDate ? a.startDate : a.dueDate);
        var bDate = assignValue(b.startDate ? b.startDate : b.dueDate);

        if (aDate > bDate)
            return -1;
        
        if (aDate < bDate)
            return 1;
        
        return 0;
    });
}

export function sortData(inputData, groups, groupsDates) {
    var root = [], children = [], waiting = [], hasGroups = false;

    if (!Timeline.Context.disableGroups && groups && groups.length) {
        groups.forEach((g, i) => {
            var group = Object.assign({}, g);
            var groupDates = groupsDates[g.id];

            if (groupDates) {
                group.startDate = groupDates.startDate;
                group.dueDate = groupDates.dueDate;
            }

            var dummySpacer = { id: `dummy${group.id}`, isDummy: true };
            root.push(dummySpacer);

            // Set unique id
            if (group && group.id.toString().indexOf('group') == -1)
                group.id = `group${group.id}`;

            // Set isGroup flag as object will work as normal item
            group.isGroup = true;

            if (Timeline.Context.viewport && group.isExpanded)
                Timeline.Context.viewport.items.expanded[group.id] = group.id;

            root.push(group);
        });

        hasGroups = true;
    } 

    var x = inputData.length - 1;
    var isCompleted = true;

    if (x < 0) isCompleted = false;

    while (isCompleted) {
        var item = inputData[x];

        if (x == 0) isCompleted = false;
        x--;
        
        if (!Timeline.Context.showCardsWithoutDates && (!item.startDate && !item.dueDate)) continue;

        if (!isNull(item.parent) && Timeline.Context.indexes[item.parent])
            children.push(item);
        else if (hasGroups) {

            var index = root.findIndex(obj => obj.isGroup && obj.id == `group${item.group}`);

            if (index != -1) {
                root.splice(index + 1, 0, item);
            }
        } else root.push(item);
    }

    children = sortByDates(children);
    for(var x = 0; x < children.length; x++) {
        var child = children[x];

        var index = root.findIndex(obj => obj.id === child.parent.toString());
        if (index == -1 && Timeline.Context.indexes[child.parent]) waiting.push(child);
        else if (index != -1) {
            root.splice(index + 1, 0, child);
            checkChildItemDates(root[index], child, root);
        }
    }

    root = loopUntilEmpty(waiting, root);

    // Clear internal references
    [children, waiting] = [null, null];

    // Return sorted collection
    return root;
}

export function loopUntilEmpty(inputArray, outputArray) {

    // Array for holding data without existing parent
    var proxyArray = [];

    for(var x = 0; x < inputArray.length; x++) {
        var item = inputArray[x];
        var index = outputArray.findIndex(obj => obj.id.toString() === item.parent.toString());

        if (index == -1) {
            proxyArray.push(item);
            continue;
        }
        
        checkChildItemDates(outputArray[index], item, outputArray);
        outputArray.splice(index + 1, 0, item);
    }

    if (proxyArray.length)
        outputArray = loopUntilEmpty(proxyArray, outputArray);

    // Clear internal references
    proxyArray = null;

    return outputArray;
}

export function checkChildItemDates(parent, child, collection) {
    if (!child.parentTree) child.parentTree = [];
    if (child.parentTree.indexOf(parent.id) == -1) child.parentTree.push(parent.id);

    checkRelationship(parent, child);

    if (!isNull(parent.parent)) {
        var index = collection.findIndex(obj => obj.id.toString() === parent.parent.toString());
        if (index != -1) checkChildItemDates(collection[index], child, collection);
    }
}

export function checkRelationship(parent, child) {
    if (parent.startDate && child.startDate && child.startDate.valueOf() <= parent.startDate.valueOf())
        if (parent.childrenStartDate && parent.childrenStartDate.valueOf() > child.startDate.valueOf()) {
            parent.childrenStartDate = child.startDate;
            if (!child.ignoreDateConflicts)
                parent.hasError = true;
        } else if (!parent.childrenStartDate) {
            parent.childrenStartDate = child.startDate;
            if (!child.ignoreDateConflicts)
                parent.hasError = true;
        }

    
    if (parent.dueDate && child.dueDate && child.dueDate.valueOf() >= parent.dueDate.valueOf())
        if (parent.childrenEndDate && parent.childrenEndDate.valueOf() < child.dueDate.valueOf()) {
                parent.childrenEndDate = child.dueDate;
                if (!child.ignoreDateConflicts)
                    parent.hasError = true;
        } else if (!parent.childrenEndDate) {
                parent.childrenEndDate = child.dueDate;
                if (!child.ignoreDateConflicts)
                    parent.hasError = true;
        }
}

export function renderFieldValue(column, value) { // dynamic from kanbo
    /*if (column.internalName == 'status') {
        if (isNull(value))
            return '<span class="status"><span class="circle notStarted"></span></span>'

        else return `<span class="status">${value == 1 ?
            '<span class="circle inProgress"></span>' :
            '<span class="circle done"></span>'}</span>`;
    }*/
    
    //if (!column.source)

    return `<span>${value}</span>`
    
}

export function adjustEndDate(data) {
    
    // Hack for proper drawing
    return data.dueDate.clone().addPeriod(-1).toDate();
}

export function createInternalStyling(name) {
    var s = document.createElement('style');
    s.innerText = ':root { --isEmpty: 1; }';
    document.head.appendChild(s);
    
    return document.styleSheets[document.styleSheets.length - 1];
}

export function checkDatesDifference(startDate, dueDate) {

    if (!startDate || !dueDate) return true;

    const { scale } = Timeline.Context;
    
    switch(scale) {
        case TIMELINESCALE.DAY:
            return startDate.isSame(dueDate, 'days');
        case TIMELINESCALE.WEEK:
            return startDate.isSame(dueDate, 'isoWeeks');
        case TIMELINESCALE.MONTH:
            return startDate.isSame(dueDate, 'months');
        case TIMELINESCALE.QUARTER:
            return startDate.isSame(dueDate, 'quarters');
        case TIMELINESCALE.HALF_YEAR:
            return getHalfYear(startDate) == getHalfYear(dueDate) &&
                startDate.isSame(dueDate, 'years');
        case TIMELINESCALE.YEAR:
            return startDate.isSame(dueDate, 'years');
    }
}
