import { uuidv4 } from '../helpers/helpers';

globalThis.Timeline.MovementBlackBox = function () {
    Timeline.movementInstances = {};
    Timeline.currentTarget = null;

    Timeline.supportedKeys = {
        ArrowUp: {
            keyDown: function () {
                Timeline.Context.viewport.handleViewportMovement({
                    change: { x: 0, y: 1 },
                    direction: { x: 0, y: 0 },
                })
            }
        },
        ArrowDown: {
            keyDown: function () {
                Timeline.Context.viewport.handleViewportMovement({
                    change: { x: 0, y: 1 },
                    direction: { x: 0, y: 1 },
                })
            }
        },
        ArrowLeft: {
            keyDown: function () {
                Timeline.Context.viewport.handleViewportMovement({
                    change: { x: 1, y: 0 },
                    direction: { x: 0, y: 0 },
                })
            }
        },
        ArrowRight: {
            keyDown: function () {
                Timeline.Context.viewport.handleViewportMovement({
                    change: { x: 1, y: 0 },
                    direction: { x: 1, y: 0 },
                })
            }
        },
    };
    
    // Attach global events
    document.onmousemove = function (args) {
        if (!Timeline.currentTarget) return;

        Timeline.movementInstances[Timeline.currentTarget].onMouseMove(args);
    }

    document.onmouseup = function (args) {
        if (!Timeline.currentTarget) return;

        Timeline.movementInstances[Timeline.currentTarget].onMouseUp(args);
    }

    // Override browser mouse wheel events ( block default browser zoom )
    Timeline.Context.rootContainer.addEventListener('wheel', onMouseWheel, { passive: false });
    Timeline.Context.rootContainer.addEventListener('mousewheel', onMouseWheel, { passive: false });
    Timeline.Context.rootContainer.addEventListener('DOMMouseScroll', onMouseWheel, { passive: false });   
        
    function onMouseWheel (args) {
        if (args.target.className == 'emptyNode' || args.target.className == 'emptyNodesList') return;

        if (args.ctrlKey) {
            args.preventDefault();
            Timeline.Context.header.changeCalendar(args, true);
        } else if (Timeline.onMouseWheel)
            Timeline.onMouseWheel(args);
    }

    document.onkeydown = function (args) {
        var key = Timeline.supportedKeys[args.key];
        if (key && key.keyDown) key.keyDown();
    }

    document.onkeyup = function (args) {
        var key = Timeline.supportedKeys[args.key];
        if (key && key.keyUp) key.keyUp();
    }

    document.ontouchmove = function (args) {
        if (!Timeline.currentTarget) return;

        Timeline.movementInstances[Timeline.currentTarget].onMouseMove(args);
    }

    document.ontouchend = function (args) {
        if (!Timeline.currentTarget) return;

        Timeline.movementInstances[Timeline.currentTarget].onMouseUp(args);
    }

    document.ontouchcancel = function (args) {
        if (!Timeline.currentTarget) return;

        Timeline.movementInstances[Timeline.currentTarget].onMouseUp(args);
    }

    this.dispose = function () {
        Timeline.Context.rootContainer.removeEventListener('wheel', onMouseWheel);
        Timeline.Context.rootContainer.removeEventListener('mousewheel', onMouseWheel);
        Timeline.Context.rootContainer.removeEventListener('DOMMouseScroll', onMouseWheel);   

        document.onmouseup = null;
        document.onmousemove = null;
        document.onkeydown = null;
        document.onkeyup = null;
        document.ontouchmove = null;
        document.ontouchend = null;
        document.ontouchcancel = null;
    }
}

globalThis.Timeline.MovementHelper = function (
    container,
    element,
    startCallback,
    actionCallback,
    finishCallback,
    calculateOutside,
    direction = null,
    attachToElement = false,
    cssClass = null,
    useZoomByChange = false,
    useZoomByPositioning = false) {

    var _this = this;

    // Set instance unique id
    _this.id = uuidv4();

    // If there is user interaction
    _this.hasUserInteraction = false;

    // Interaction start position
    _this.startPosition = { x: 0, y: 0 };

    // Change made by last interaction
    _this.lastChange = { x: 0, y: 0 };

    // Mouse position by last interaction
    _this.lastMousePosition;

    // If movement event is blocked
    _this.passTrought = false;

    // Initialize movement helper
    _this.init = function () {

        // Store intance
        Timeline.movementInstances[_this.id] = _this;

        // Attach nessecary events
        _this.attachEvents();
    }

    _this.attachEvents = function () {
        if (attachToElement) {
            element.onmousedown = _this.onMouseDown;
            element.ontouchstart = _this.onMouseDown; 
            return;
        }

        container.onmousedown = _this.onMouseDown;
        container.ontouchstart = _this.onMouseDown;
    }

    _this.onMouseDown = function (args) {
        args.stopPropagation();
        args.preventDefault();

        if (_this.passTrought)
            _this.passTrought = false;

        // Get current interaction ( mouse / touch ) position
        var [clientX, clientY] = _this.getInteractionCoordinates(args);

        // Set current interaction target
        Timeline.currentTarget = _this.id;

        // Unblock interaction events
        _this.hasUserInteraction = true;

        // Set new start position
        _this.startPosition = { x: clientX, y: clientY };

        // Set custom styling for current interaction target
        if (cssClass) element.classList.add(cssClass);

        // Make callback to parent component
        if (startCallback) startCallback(args);
    }

    _this.onMouseMove = function (args) {

        // If no user interaction ( clear mouse movement )
        if (!_this.hasUserInteraction && !_this.passTrought) return;

        // If no button used...
        if (!args.buttons && !args.touches  && !_this.passTrought) {
            _this.onMouseUp(args);
            return;
        }

        args.stopPropagation();
        if (args.defaultPrevented)
            args.preventDefault();

        // Calculate user interaction change
        var gridData = _this.calculateGridChange(args);

        // Make callback to parent component
        if (actionCallback) actionCallback(gridData);
    }

    _this.onMouseUp = function (args) {
        args.stopPropagation();
        args.preventDefault();
        //if (!args.defaultPrevented)
            
        
        if (_this.hasUserInteraction) {
            _this.hasUserInteraction = false;

            // Reset global movement target
            Timeline.currentTarget = null;

            if (cssClass) element.classList.remove(cssClass);

            if (finishCallback) finishCallback({
                default: args,
                startPosition: _this.startPosition
            });

            // Reset events data
            _this.startPosition = { x: 0, y: 0 };
            _this.lastChange = { x: 0, y: 0 };
            _this.lastMousePosition = null;
            
            if (cssClass) element.classList.remove(cssClass);

            if (finishCallback) finishCallback({
                default: args
            });
        }
    }

    _this.calculateGridChange = function (args) {
        if (!_this.lastMousePosition) _this.lastMousePosition = _this.startPosition;

        var [clientX, clientY] = _this.getInteractionCoordinates(args);

        var newPosition = {
            x: clientX,
            y: clientY
        };

        var change = {
            x: Math.round(Math.abs((_this.startPosition.x - clientX) / Timeline.Config.cellWidth)),
            y: Math.round(Math.abs((_this.startPosition.y - clientY) / Timeline.Config.cellHeight))
        };

        var currentChange = {
            x: Math.abs(change.x - _this.lastChange.x),
            y: Math.abs(change.y - _this.lastChange.y)
        };
        
        var direction = {
            x: _this.lastMousePosition.x > clientX,
            y: _this.lastMousePosition.y > clientY
        };

        _this.lastMousePosition = newPosition;
        _this.lastChange = change;

        return {
            startPosition: _this.startPosition,
            mouse: newPosition,
            change: currentChange,
            direction: direction,
            path: args.composedPath(),
            default: args
        };
    }

    _this.getInteractionCoordinates = function (args) {
        var clientX = !args.clientX && args.touches && args.touches.length
                        ? args.touches[0].clientX : args.clientX,
            clientY = !args.clientY && args.touches && args.touches.length
                        ? args.touches[0].clientY : args.clientY;

        return [clientX, clientY];
    }
}