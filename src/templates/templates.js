globalThis.Timeline.Templates = {
    headerTemplate:
        '<header>' +
            '<groups><button>Group</button></groups>' +
            '<filters><button>Sort</button></filters>' +
            '<add>' +
                '<button title="Day" data-scale="0" class="selected">Add</button>' +
            '</add>' +
            '<views>' +
                '<button title="Day" data-scale="0" class="selected">Day</button>' +
                '<button title="Week" data-scale="1">Week</button>' +
                '<button title="Month" data-scale="2">Month</button>' +
                '<button title="Quarter" data-scale="3">Quarter</button>' +
                '<button title="Half Year" data-scale="4">Half Year</button>' +
                '<button title="Year" data-scale="5">Year</button>' +
                '<button title="Control Menu"></button></views>' +
        '</header>',

    listTemplate: 
        '<listHeader>' +
            '<search><input type="text" id="searchBox" autocomplete="off" placeholder="Search for..." /></search>' +
            '<gridHeader></gridHeader>' +
            '<pinList></pinList>' +
            '<resize class="leftpanel"></resize>' +
        '</listHeader>',

    gridColumnTemplate:
        '<span><span>{0}</span><resize class="column" data-count="{1}"></resize></span>',

    viewportTemplate:
        '<div id="viewport"></div>',

    columnsTemplate: '<columns style="width: {0};"></columns>',
    columnTemplate: '<column id="{0}" class="{3}"><header><div>{2}{1}</div></header></column>',

    connectionsTemplate: '<connections></connections>',

    masksTemplate: '<masks></masks>',
    maskTemplate: '<mask id="{0}" data-childId="{1}"></mask>',

    itemsTemplate: '<items></items>',
    itemTemplate:
        '<item id="{0}" data-id="{0}" class="{4}" style="--left: var(--{2}); --right: var(--{3}); --progress: {7}px; --progressMargin: {8}px;">' +
            '<header>' +
                '<span>{6}</span>' +
                '<user></user>' +
                '<expand></expand>' +
            '</header>' +
            '<indicator style="width: calc(50px - var(--right));" class="left"></indicator>' +
            '<indicator style="width: calc(calc(calc(100% - 50px) - var(--left)) * -1);" class="right"></indicator>' +
            '<shape style="left: var(--left); right: calc(calc(100% - var(--right)));">' +
                '<attachLeft></attachLeft>' +
                '<resizeLeft></resizeLeft>' +
                '<container style="{5}">' +
                    '<expand></expand>' +
                    '<span>{1}</span>' +
                    '<user></user>' +
                    '<edit></edit>' +
                '</container>' +
                '<attachRight></attachRight>' +
                '<resizeRight></resizeRight>' +
            '</shape>' +            
        '</item>',

    emptyItemTemplate:
        '<item id="{0}" data-id="{0}">' +
            '<header>' +
                '<span>{1}</span>' +
                '<user></user>' +
                '<expand></expand>' +
            '</header>' +
            '<div class="noDate-setOne"></div>' +
        '</item>',

    dummyItemTemplate:
        '<item id="{0}" data-id="{0}" class="dummy"></item>',

    groupTemplate:
        '<item data-hasChildren="true" data-hasVisibleChildren="false" id="{0}" data-id="{0}" class="{2} group" style="border-bottom-color: {3}">' +
            '<header style="border-bottom-color: {3}">' +
                '<span class="empty">' +
                '{1}' +
                '<add></add>' +
                '</span>' +
                '<user></user>' +
                '<expand></expand>' +
            '</header>' +
            '<div class="title">' + 
            '<expand></expand>' +
            '{1}' +
            '</div>' +
        '</item>',

    groupProgressTemplate:
        '<item data-hasChildren="true" data-hasVisibleChildren="false" id="{0}" data-id="{0}" class="{2} group" style="--left: var(--{4}); --right: var(--{5}); --progress: {6}px; --progressMargin: {7}px; border-bottom-color: {3}; --progressColor: {3};">' +
            '<header style="border-bottom-color: {3}">' +
                '<span class="empty">' +
                '{1}' +
                '{8}' +
                '</span>' +
                '<user></user>' +
                '<expand></expand>' +
            '</header>' +
            '<indicator style="width: calc(50px - var(--right));" class="left"></indicator>' +
            '<indicator style="width: calc(calc(calc(100% - 50px) - var(--left)) * -1);" class="right"></indicator>' +
            '<shape style="left: var(--left); right: calc(calc(100% - var(--right))); background-color: {3}80 !important;">' +
                '<container style="{9}">' +
                    '<expand></expand>' +
                    '<span>{1}</span>' +
                '</container>' +
            '</shape>' +
        '</item>',

    floatingBoxTemplate:
        '<fbox id={0}>' +
            '<title>{1}</title>' +
            '<button class="btnClose">x</button>' +
            '<content>{2}</content>' +
        '</fbox>',

    groupPanelTemplate: 
        '<div>' +
            '<div><button id="applyBtn">Apply</button><button>Reset</button></div>' +
        '</div>',

    emptyNodesListTemplate:
        '<div class="emptyNodesList">{0}</div>',

    emptyNodeTemplate:
        '<div data-id="{0}" class="emptyNode">{1}</div>'
}
