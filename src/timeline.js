import moment from 'moment';
import { ROWHEIGHT, TIMELINESCALE } from './constants/enums';
import { sortData, createIndexes, sortByDates } from './helpers/helpers';
import { initializeEvents } from './helpers/events';

var locale = moment.locale();
moment.defineLocale('en-gantt', {
    parentLocale: 'en',
    week: {
        dow : 1, // Monday is the first day of the week.
      }
});
moment.locale(locale);

globalThis.Timeline = function (container, initialDate) {
    
    var _this = this;

    Timeline.Context = _this;                           // Set global timeline context
    Timeline.Config;                                    // Control configuration

    _this.rootContainer = container;                    // Set control root container
    _this.today = moment().locale('en-gantt');                 // Today date holder
    _this.initialDate = initialDate ?                   // Timeline initial date
                        moment(initialDate).locale('en-gantt') : _this.today;

    _this.initialDate = _this.initialDate.set({hour:0,minute:0,second:0,millisecond:0});

    _this.currentDate = _this.initialDate;

    _this.header;                                       // Control header
    _this.listHeader;                                   // Items list header
    _this.viewport;                                     // Control viewport
    _this.search;                                       // Search box

    _this.indexes = null;

    _this.viewColumns = [];                         

    _this.initialized = false;

    _this.data;                                         // Nodes data
    _this.groups;                                       // Current groups collection
    _this.sources = {};                                      // Current c
    
    _this.emptyNodes;                                   // Nodes without dates
    _this.connectionsData;                              // Connections data
    _this.loadData;                                     // Promise: load nods data
    _this.loadConnections;                              // Promise: load connections data
    
    _this.movementBlackBox;                             // Global movement helper / Movement blackbox

    _this.rowHeight = ROWHEIGHT.m;
    _this.scale = TIMELINESCALE.DAY;                    // Current timeline scale -- Days are default
    _this.skipWeekends = false;
    _this.ensurePrevNext = true;
    _this.showUserAvatar = true;
    _this.showCardsWithoutDates = true;
    _this.showGroupsProgress = true;

    _this.oldDisableGroups = false;
    _this.disableGroups = false;

    // Timeline events
    _this.events;

    _this.hasGroups = false;

    // _this.dynamicInteraction = false;

    _this.init = function () {

        // Initialize control events
        initializeEvents();

        // Set root css class
        _this.rootContainer.classList.add('timeline');

        // Load configuration
        Timeline.Config = new Timeline.Configuration();

        // Initialize movement blackbox
        _this.movementBlackBox = new Timeline.MovementBlackBox();
        
        // Attach global events
        window.onresize = _this.handleWindowResize;

        _this.rootContainer.appendChild(document.createElement('loader'));

        _this.initalized = true;

        if (_this.data) {
            _this.rootContainer.children[0].remove();
            _this.initializeView();
        }

        if (!_this.loadData) return;

        // Load data
        _this.loadData().then(function (result) {
            var groupsDates;
            [_this.indexes, _this.emptyNodes, groupsDates] = createIndexes(result);
            _this.data = sortData(result, _this.groups, groupsDates);
            if (_this.groups) _this.hasGroups = true;
            
            _this.loadConnections().then(function (result) {
                _this.connectionsData = result;

                _this.rootContainer.children[0].remove();
                _this.initializeView();
            });
        });
    }

    _this.updateData = function (data, connections) {
        _this.data = data;
        _this.connectionsData = connections;

        _this.refresh();
    }

    _this.initializeView = function () {

        // Initialize control components
        _this.header = new Timeline.Header(_this);
        _this.listHeader = new Timeline.ListHeader();
        _this.viewport = new Timeline.Viewport(_this);

        _this.listHeader.init();
        _this.viewport.init();

        _this.oldDisableGroups = _this.disableGroups;
    }

    _this.bingIntoView = function (id) {
        console.log('bring into view call response');
    }

    _this.setRowHeight = function (rowHeight) {

        // Save new setting
        _this.rowHeight = ROWHEIGHT[rowHeight];

        // Save as config entry
        Timeline.Config.cellHeight = _this.rowHeight;

        if (rowHeight == 'xl')
            _this.rootContainer.classList.add('xl');
        else {
            _this.rootContainer.classList.remove('xl')
        }

        // Control hard refresh
        _this.refresh(true);
    }

    _this.setData = function (nodes, connections, groups, sources) {

        //var nodes = [...nodesInput];

        var needRefresh = false;
        var needHardRefresh = false;
        var groupsDates;

        if (!_this.data && _this.initalized) needHardRefresh = true;
        if (_this.data && _this.initalized) needRefresh = true;

        if (nodes) {
            if (_this.disableGroups) nodes = sortByDates(nodes);

            [_this.indexes, _this.emptyNodes, groupsDates] = createIndexes(nodes);

            if (!Timeline.Context.disableGroups && groups) {
                _this.groups = groups;
                _this.hasGroups = true;
            }

            _this.data = sortData(nodes, groups, groupsDates);
        }

        if (connections)
            _this.connectionsData = connections;

        if (sources)
            _this.sources = sources;

        if (_this.needHardRefresh) {
            if (_this.data) {
                _this.rootContainer.children[0].remove();
                _this.initializeView();
            }
        } else if (needRefresh) _this.refresh();
    }

    _this.changeScale = function (scale) {
        Timeline.Context.header.changeCalendar(null, null, scale);
    }

    _this.showNodesWithoutDates = function (close) {
        _this.header.showDialogBox(1);
    }

    _this.handleWindowResize = function (args) {        
        _this.refresh();
    }

    _this.refresh = function (hardRefresh) {
        
        if (hardRefresh) {

            // Rise global event
            _this.events.onClean({});

            // Clear all object in memory
            _this.viewport.items.elements = {};
            _this.viewport.connections.elements = {};

            // TODO: need to be clened or restored
            //_this.viewport.items.expanded = {};
            //_this.viewport.items.masks = {}
            

            _this.viewport.items.underground.innerHTML = '';

            // Clean DOM
            _this.listHeader.gridHeader.innerHTML = '';


            // Recreate control header ( grid columns )
            _this.listHeader.createGridHeader();
        }
        
        _this.viewport.readViewportSize();

        if (_this.oldDisableGroups != _this.disableGroups) {
            _this.oldDisableGroups = _this.disableGroups;
            Object.keys(Timeline.Context.viewport.items.elements).forEach((e, i) => {
                Timeline.Context.viewport.items.elements[e].isRemoved = true;
            });
        }

        _this.events.onClean({});

        _this.viewport.columns.clear();
        _this.viewport.columns.render(_this.currentDate, true);
        
        _this.viewport.items.clear();
        _this.viewport.items.render();

        _this.viewport.connections.clear();
        _this.viewport.connections.render();

        _this.viewport.update();
    }
}